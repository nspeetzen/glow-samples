#pragma once

#include <glow/fwd.hh>

#include <glow-extras/glfw/GlfwApp.hh>
#include <glow-extras/vector/backend/opengl.hh>

class Vector2DSample : public glow::glfw::GlfwApp
{
private:
    float mRuntime = 0.0f;
    bool mAnimate = true;

    glow::vector::OGLRenderer mVectorRenderer;

protected:
    void init() override;
    void render(float elapsedSeconds) override;
};
