#include "MassivePointsSample.hh"

#include <glow/objects/Program.hh>
#include <glow/objects/Shader.hh>
#include <glow/objects/VertexArray.hh>

#include <glow/common/scoped_gl.hh>
#include <glow/common/str_utils.hh>

#include <glow-extras/geometry/Cube.hh>

#include <AntTweakBar.h>

using namespace glow;
using namespace glow::camera;

static void TW_CALL ButtonRebuild(void* data)
{
    ((MassivePointsSample*)data)->rebuild();
}

void MassivePointsSample::init()
{
    setGui(GlfwApp::Gui::AntTweakBar);
    GlfwApp::init(); // call to base!

    mShader = Program::createFromFile(util::pathOf(__FILE__) + "/shader");

    rebuild();

    auto cam = getCamera();
    cam->setLookAt({12, 12, 12}, {0, 0, 0});

    TwAddVarRW(tweakbar(), "Points Per Side", TW_TYPE_INT32, &mPointsPerSide, "");
    TwAddButton(tweakbar(), "Rebuild", ButtonRebuild, this, "");
}

void MassivePointsSample::render(float elapsedSeconds)
{
    auto cam = getCamera();

    GLOW_SCOPED(clearColor, 0, 0, 0, 1);
    GLOW_SCOPED(enable, GL_DEPTH_TEST);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    auto shader = mShader->use();
    shader.setUniform("uViewProj", cam->getProjectionMatrix() * cam->getViewMatrix());

    mPoints->bind().draw();
}

void MassivePointsSample::rebuild()
{
    struct Vertex
    {
        glm::tvec3<uint8_t> pos;
    };

    std::vector<Vertex> vertices;
    vertices.reserve(mPointsPerSide * mPointsPerSide * mPointsPerSide);
    for (auto z = 0; z < mPointsPerSide; ++z)
        for (auto y = 0; y < mPointsPerSide; ++y)
            for (auto x = 0; x < mPointsPerSide; ++x)
            {
                Vertex v;
                v.pos.x = x;
                v.pos.y = y;
                v.pos.z = z;
                vertices.push_back(v);
            }
    glow::info() << "stride " << sizeof(Vertex);
    auto ab = ArrayBuffer::create();
    // ab->defineAttribute<int>("aPosition");
    ab->defineAttribute("x", GL_UNSIGNED_BYTE, 1, AttributeMode::NormalizedInteger);
    ab->defineAttribute("y", GL_UNSIGNED_BYTE, 1, AttributeMode::NormalizedInteger);
    ab->defineAttribute("z", GL_UNSIGNED_BYTE, 1, AttributeMode::NormalizedInteger);
    ab->setStride(sizeof(Vertex));
    // auto ab = ArrayBuffer::create({
    //     {&Vertex::pos, "aPosition"}, //
    // });
    ab->bind().setData(vertices);
    mPoints = VertexArray::create(ab, GL_POINTS);
}
