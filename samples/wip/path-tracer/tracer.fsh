in vec2 vPosition;

uniform mat4 uInvProj;
uniform mat4 uInvView;
uniform vec3 uCamPos;
uniform uint uSeed;
uniform vec2 uViewport;
uniform int uMaxBounces;

out vec3 fColor;

// =============================================================================
// settings

const float MAX_DISTANCE = 500;
const float EPSILON = 0.001;
const float PI = 3.14159265359;

// =============================================================================
// Helper
vec3 igamma(vec3 color) {
  return pow(color, vec3(2.2));
}
vec3 igamma(float r, float g, float b) {
  return igamma(vec3(r, g, b));
}

uint wang_hash(uint seed)
{
  seed = (seed ^ 61) ^ (seed >> 16);
  seed *= 9;
  seed = seed ^ (seed >> 4);
  seed *= 0x27d4eb2d;
  seed = seed ^ (seed >> 15);
  return seed;
}

float wang_float(uint hash)
{
  return hash / float(0x7FFFFFFF) / 2.0;
}

float uniformFloat(float min, float max, inout uint random) {
  random = wang_hash(random);
  return (max - min) * wang_float(random) + min;
}

vec2 uniformVec2(vec2 min, vec2 max, inout uint random) {
  float x = uniformFloat(min.x, max.x, random);
  float y = uniformFloat(min.y, max.y, random);
  return vec2(x, y);
}

vec3 directionCosTheta(vec3 normal, inout uint random) {
  float u1 = uniformFloat(0, 1, random);
  float phi = uniformFloat(0, 2 * PI, random);

  float f = sqrt(1 - u1);

  float x = f * cos(phi);
  float y = f * sin(phi);
  float z = sqrt(u1);

  vec3 xDir = abs(normal.x) < abs(normal.y) ? vec3(1, 0, 0) : vec3(0, 1, 0);
  vec3 yDir = normalize(cross(normal, xDir));
  xDir = cross(yDir, normal);
  return xDir * x + yDir * y + z * normal;
}

vec3 directionUniform(vec3 normal, inout uint random) {
  float u1 = uniformFloat(0, 1, random);
  float phi = uniformFloat(0, 2 * PI, random);
  float f = sqrt(1 - u1 * u1);

  float x = f * cos(phi);
  float y = f * sin(phi);
  float z = u1;

  vec3 dir = vec3(x, y, z);
  dir *= sign(dot(dir, normal));

  return dir;
}

// =============================================================================
// Material

struct Material {
  vec3 diffuseColor;
  vec3 specularColor;
  float roughness;
  float refractiveness;
  float eta;
};

const vec3 dielecSpec = vec3(0.04);

// GGX BRDF, no cos(theta), no div by PI
float GGX(vec3 N, vec3 V, vec3 L, float roughness, float F0)
{
    // see http://www.filmicworlds.com/2014/04/21/optimizing-ggx-shaders-with-dotlh/
    vec3 H = normalize(V + L);

    float dotLH = max(dot(L, H), 0.0);
    float dotNH = max(dot(N, H), 0.0);
    float dotNL = max(dot(N, L), 0.0);
    float dotNV = max(dot(N, V), 0.0);

    float alpha = roughness * roughness;

    // D (GGX normal distribution)
    float alphaSqr = alpha * alpha;
    float denom = dotNH * dotNH * (alphaSqr - 1.0) + 1.0;
    float D = alphaSqr / (PI * denom * denom);
    // pi because BRDF

    // F (Fresnel term)
    float F_a = 1.0;
    float F_b = pow(1.0 - dotLH, 5); // manually?
    float F = mix(F_b, F_a, F0);

    // G (remapped hotness, see Unreal Shading)
    float k = (alpha + 2 * roughness + 1) / 8.0;
    float G = dotNL / (mix(dotNL, 1, k) * mix(dotNV, 1, k));
    // '* dotNV' - canceled by normalization


    // '/ dotLN' - canceled by lambert NOT
    // '/ dotNV' - canceled by G
    return D * F * G / 4.0 / dotNL;
}
// GGX for Shading, includes cos(theta), no div by PI
float shadingGGX(vec3 N, vec3 V, vec3 L, float roughness, float F0)
{
    // see http://www.filmicworlds.com/2014/04/21/optimizing-ggx-shaders-with-dotlh/
    vec3 H = normalize(V + L);

    float dotLH = max(dot(L, H), 0.0);
    float dotNH = max(dot(N, H), 0.0);
    float dotNL = max(dot(N, L), 0.0);
    float dotNV = max(dot(N, V), 0.0);

    float alpha = roughness * roughness + 0.0001;

    // D (GGX normal distribution)
    float alphaSqr = alpha * alpha;
    float denom = dotNH * dotNH * (alphaSqr - 1.0) + 1.0;
    float D = alphaSqr / (denom * denom);
    // NO pi because BRDF -> lighting

    // F (Fresnel term)
    float F_a = 1.0;
    float F_b = pow(1.0 - dotLH, 5); // manually?
    float F = mix(F_b, F_a, F0);

    // G (remapped hotness, see Unreal Shading)
    float k = (alpha + 2 * roughness + 1) / 8.0;
    float G = dotNL / (mix(dotNL, 1, k) * mix(dotNV, 1, k));
    // '* dotNV' - canceled by normalization


    // '/ dotLN' - canceled by lambert
    // '/ dotNV' - canceled by G
    return max(0.0, min(10, D * F * G / 4.0));
}

// =============================================================================
// Geometry

struct Sphere {
  vec3 center;
  float radius;
  Material material;
};

const float FAR = 1000;
const float BW = 2.5;
const Sphere[] spheres = Sphere[](
  // walls
  // left
  Sphere( vec3(-FAR - BW, 0, 0), FAR, Material(igamma(1, 0, 0), dielecSpec, 0.7, 0.0, 1.0) ),
  // right
  Sphere( vec3(+FAR + BW, 0, 0), FAR, Material(igamma(0, 1, 0), dielecSpec, 0.7, 0.0, 1.0) ),
  // front
  Sphere( vec3(0, 0, -FAR - BW), FAR, Material(igamma(vec3(0.7)), dielecSpec, 0.7, 0.0, 1.0) ),
  // back
  Sphere( vec3(0, 0, +FAR + BW), FAR, Material(igamma(vec3(0.7)), dielecSpec, 0.7, 0.0, 1.0) ),
  // bottom
  Sphere( vec3(0, -FAR - BW, 0), FAR, Material(igamma(vec3(0.3)), dielecSpec, 0.7, 0.0, 1.0) ),
  // top
  Sphere( vec3(0, +FAR + BW, 0), FAR, Material(igamma(vec3(0.7)), dielecSpec, 0.7, 0.0, 1.0) ),



  // metal sphere
  Sphere( vec3(-.8, -BW + .6, -.8), .6, Material(vec3(0), vec3(1, 0, 0), 0.3, 0.0, 1.0) ),
  // reflective sphere
  Sphere( vec3(-.8, -BW + .7, +.8), .7, Material(vec3(0), vec3(1, 1, 1), 0.0, 0.0, 1.0) ),
  // refractive sphere
  Sphere( vec3(+.8, -BW + .7, +.8), .7, Material(vec3(0), vec3(0), 0.0, 1.0, 1.05) ),
  // sphere
  Sphere( vec3(+1.1, -BW + .1, +1.1), .1, Material(igamma(1, 0, 1), dielecSpec, 0.7, 0.0, 1.0) ),
  // sphere
  Sphere( vec3(+.8, -BW + .5, -.8), .5, Material(igamma(0, 0, 1), dielecSpec, 0.7, 0.0, 1.0) )
);

// =============================================================================
// Lights

struct SphereLight {
  vec3 center;
  float radius;
  vec3 color;
};

const SphereLight[] lights = SphereLight[](
  SphereLight( vec3(0, BW - 0.5, -.9), .3, vec3(1) )
);

// =============================================================================
// intersections

struct Intersection {
  vec3 pos;
  vec3 normal;
  Material material;
};

// normalized dir
// origin with offset away from surface
bool intersect(vec3 origin, vec3 dir, float maxLength, out Intersection intersection)
{
  int maxI = spheres.length();
  float l = maxLength;

  for (int i = 0; i < maxI; ++i) {
    Sphere s = spheres[i];

    vec3 d = origin - s.center;
    float dotDD = dot(d, dir);

    float a = dotDD * dotDD - dot(d, d) + s.radius * s.radius;
    if (a < 0.0)
      continue; // no intersection

    float sa = sqrt(a);
    float t = -dotDD - sa;
    if (t < EPSILON)
      t = -dotDD + sa;

    if (t > EPSILON && t < l) {
      l = t;
      intersection.pos = origin + dir * t;
      intersection.normal = (intersection.pos - s.center) / s.radius;
      intersection.material = s.material;
    }
  }

  // move intersection away a bit
  // TODO: is this required?
  // intersection.pos += EPSILON * intersection.normal;

  return l < maxLength;
}

// =============================================================================
// Illumination

// direct illu at a given point
// inDir points TOWARDS the surface
float directIllumination(vec3 pos, vec3 inDir, vec3 N, vec3 colorFilter, Material material) {
  Intersection intr;
  float color = 0;

  float specularColor = dot(material.specularColor, colorFilter);
  float diffuseColor = dot(material.diffuseColor, colorFilter) * (1 - specularColor);

  int lightCnt = lights.length();
  for (int i = 0; i < lightCnt; ++i) {
    SphereLight l = lights[i];

    vec3 V = -inDir;
    vec3 L = l.center - pos;
    float lightDis = length(L);
    L /= lightDis;

    if (intersect(pos, L, lightDis, intr))
      continue;

    // diffuse
    color += max(0.0, dot(L, N)) * dot(l.color, colorFilter) * diffuseColor;

    // specular
    color += shadingGGX(N, V, L, material.roughness, specularColor);
  }

  return color;
}

// =============================================================================
// tracing
float trace(vec3 origin, vec3 startdir, vec3 colorFilter, inout uint random)
{
  Intersection intr;
  int bounces = uMaxBounces;

  vec3 pos = origin;
  vec3 dir = startdir;
  float weight = 1;

  for (int b = 0; b < bounces; ++b) {

    // step 1: intersect with scene
    if (!intersect(pos, dir, MAX_DISTANCE, intr))
      return 0; // TODO: cubemap

    // step 2: russian roulette
    float rhoS = dot(colorFilter, intr.material.specularColor);
    float rhoD = dot(colorFilter, intr.material.diffuseColor);
    float rhoR = intr.material.refractiveness;

    // REFLECT glossy
    if (uniformFloat(0, 1, random) <= rhoS)
    {
      dir = reflect(dir, intr.normal);
      pos = intr.pos;
      weight *= 1; // specular
    }
    // REFLECT diffuse
    else if (uniformFloat(0, 1, random) <= rhoD)
    {
      dir = directionCosTheta(intr.normal, random);
      pos = intr.pos;
      weight *= 1; // cos theta vs p(w), f vs. rho, pi in p(w) and f
    }
    // REFRACT
    else if (uniformFloat(0, 1, random) <= rhoR)
    {
      float inside = sign(dot(dir, intr.normal)); // 1 for inside, -1 for outside
      dir = refract(dir, -inside * intr.normal, inside < 0 ? 1 / intr.material.eta : intr.material.eta);
      pos = intr.pos;
      weight *= 1; // refractive
    }
    // EMIT
    else
    {
      float color = directIllumination(intr.pos, dir, intr.normal, colorFilter, intr.material);
      return weight * color / (1 - rhoS) / (1 - rhoD) / (1 - rhoR);
    }
  }

  // last bounce
  float color = directIllumination(intr.pos, dir, intr.normal, colorFilter, intr.material);
  return weight * color;
}

// =============================================================================
// Main
void main() {
  uint random = wang_hash(wang_hash(uSeed + uint(gl_FragCoord.x) * 7) + uint(gl_FragCoord.y) * 15001);
  vec2 subpixel = uniformVec2(vec2(-1), vec2(1), random) / uViewport;

  vec4 near = uInvProj * vec4(vPosition * 2 - 1 + subpixel, 0.0, 1);
  vec4 far  = uInvProj * vec4(vPosition * 2 - 1 + subpixel, 0.5, 1);

  near /= near.w;
  far  /= far.w;

  near = uInvView * near;
  far  = uInvView * far;

  vec3 dir = normalize((far - near).xyz);

  float r = trace(uCamPos, dir, vec3(1, 0, 0), random);
  float g = trace(uCamPos, dir, vec3(0, 1, 0), random);
  float b = trace(uCamPos, dir, vec3(0, 0, 1), random);

  fColor = vec3(r, g, b);
}
