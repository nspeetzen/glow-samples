#glow pipeline depthPre

//#include <glow-pipeline/pass/depthpre/depthPreGpuClustering.glsl>
#include <glow-pipeline/pass/depthpre/depthPre.glsl>

in VS_OUT {
	vec2 texCoord;
	vec3 normal;
	vec3 tangent;

	vec3 fragPosWS; // World Space
	vec4 fragPosVS; // View Space
	vec4 fragPosSS; // Screen Space

	vec3 tangentCamPos;
	vec3 tangentFragPos;

	vec4 HDCPosition;
	vec4 prevHDCPosition;
} vIn;

void main()
{
    //outputDepthPreGeometry(vIn.fragPosVS.z);
    outputDepthPreGeometry();
}
