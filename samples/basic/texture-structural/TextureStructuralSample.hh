#pragma once

#include <vector>

#include <glm/ext.hpp>

#include <glow/fwd.hh>
#include <glow/gl.hh>

#include <glow-extras/glfw/GlfwApp.hh>

enum class Technique
{
    None,
    ColorOnly,
    NormalOnly,
    HeightOnly,
    Bump,
    Normal,
    Parallax,
};

class TextureStructuralSample : public glow::glfw::GlfwApp
{
public:
    TextureStructuralSample() : GlfwApp(Gui::AntTweakBar) {}

private:
    glow::SharedProgram mShader;
    glow::SharedVertexArray mCube;

    glow::SharedTexture2D mTexColor;
    glow::SharedTexture2D mTexNormal;
    glow::SharedTexture2D mTexHeight;

    float mApparentDepth = 0.05f;
    float mDisplacement = 0.05f;

    float mAmbient = 0.05f;
    float mDiffuse = 0.5f;
    float mSpecular = 0.5f;
    float mShininess = 32.0f;

    float mNormalLODBias = 0.0f;

    float mRuntime = 0.0f;
    bool mAnimate = false;

    bool mShowNormals = false;
    bool mShowWireframe = false;

    int mParallaxSamples = 16;
    float mTessellationLevel = 1;

    Technique mTechnique = Technique::None;

    glm::vec3 mLightDir = normalize(glm::vec3(-1, 5, 2));
    float mLightDis = 3.0f;

    bool mUseMicrofacetModel = false;
    bool mDeriveNormals = false;
    bool mDeriveTangents = false;

protected:
    void init() override;
    void render(float elapsedSeconds) override;
};
