#pragma once

#include <vector>

#include <glm/ext.hpp>

#include <glow/fwd.hh>
#include <glow/gl.hh>

#include <glow-extras/glfw/GlfwApp.hh>

class TextureParametersSample : public glow::glfw::GlfwApp
{
public:
    TextureParametersSample() : GlfwApp(Gui::AntTweakBar) {}

private:
    glow::SharedProgram mShader;
    glow::SharedVertexArray mCube;

	std::vector<glow::SharedTexture2D> mTextures;

	int mCurrentTexture = 0;

	GLenum mMinFilter = GL_LINEAR_MIPMAP_LINEAR;
	GLenum mMagFilter = GL_LINEAR;
	GLenum mWrap = GL_REPEAT;
	GLfloat mLODBias = 0.0f;
	GLfloat mLODMin = -10;
	GLfloat mLODMax = 10;
	GLfloat mAnsitropicFiltering = 16.0f;

	float mTextureScale = 1.0f;
	float mTextureOffset = 0.0f;

    float mRuntime = 0.0f;
    bool mAnimate = true;

protected:
    void init() override;
    void render(float elapsedSeconds) override;
};
