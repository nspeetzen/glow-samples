cmake_minimum_required(VERSION 3.0)
project(Viewer)

file(GLOB_RECURSE SHADER_FILES "shaders/*.*")

file(GLOB_RECURSE SOURCES "*.cc" "*.hh" "*.*sh")

add_executable(${PROJECT_NAME} ${SOURCES})
target_link_libraries(${PROJECT_NAME} PUBLIC glow glow-extras glfw polymesh)
target_compile_options(${PROJECT_NAME} PUBLIC ${GLOW_SAMPLES_DEF_OPTIONS})
set_property(TARGET ${PROJECT_NAME} PROPERTY FOLDER "Samples/Basic")

if (GLOW_EXTRAS_EMBED_SHADERS)
    target_compile_definitions(${PROJECT_NAME} PUBLIC GLOW_SAMPLES_VIEWER_EMBED_SHADERS)
endif()
