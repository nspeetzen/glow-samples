#include <imgui/imgui.h>
#include <imgui/imguizmo.h>

#include <glow/common/str_utils.hh>
#include <glow/data/TextureData.hh>
#include <glow/objects/TextureCubeMap.hh>
#include <glow/util/AsyncTextureLoader.hh>

#include <polymesh/algorithms/normalize.hh>
#include <polymesh/formats.hh>
#include <polymesh/objects/quad.hh>

#include <glow-extras/glfw/GlfwContext.hh>
#include <glow-extras/vector/graphics2D.hh>
#include <glow-extras/vector/image2D.hh>
#include <glow-extras/viewer/canvas.hh>
#include <glow-extras/viewer/picking.hh>
#include <glow-extras/viewer/view.hh>

#include <GLFW/glfw3.h>

#include <typed-geometry/tg.hh>

// path to sample files
std::string const dataPath = glow::util::pathOf(__FILE__) + "/../../../data/";


void simple_view(pm::vertex_attribute<tg::pos3> const& pos)
{
    // the simplest possible view: just a mesh
    gv::view(pos);
}

void basic_concepts(pm::vertex_attribute<tg::pos3> const& pos)
{
    // gv::view(obj, args...) takes some object and a list of arguments
    // - the object is converted via make_renderable(obj) into a gv::SharedRenderable
    // - for each arg, configure(r, arg) is called (where r is the created gv::Renderable&)
    // - arguments can change the renderable, the scene, or global settings (see advanced_config function for more)
    // - most arguments compose, i.e. you can specify as many as you want in gv::view(...)

    // gv::view(...); opens a view at the end of the statement
    // auto v = gv::view(...); opens the view when `v` is destroyed
    // - all views created while `v` is alive are added to the same scene
    // - auto g = gv::grid(); creates a container view and views are added as grid cells while `g` is alive

    auto g = gv::grid();

    // a named view (shown in lower-left corner)
    gv::view(pos, "Suzanne");

    // no grid
    gv::view(pos, gv::no_grid);

    // colored mesh
    gv::view(pos, tg::color3::red);

    // different object
    gv::view(tg::sphere3::unit);

    // multiple objects in the same scene
    {
        auto v = gv::view(pos);
        gv::view(pos, tg::translation(2.f, 0.f, 0.f)); // a matrix can be passed to transform the object
        gv::view(tg::sphere3::unit, tg::translation(-2.f, 0.f, 0.f) * tg::scaling(tg::size3(0.5f, 1.5, 1.f)));
    }
}

void advanced_objects(pm::vertex_attribute<tg::pos3> const& pos)
{
    tg::rng rng; // RNG for creating some random sample values
    pm::Mesh const& m = pos.mesh();

    // by default, gv::view(obj, ...) chooses gv::make_renderable(obj) to create a renderable
    // this can be overridden by gv::polygons, gv::lines, gv::points

    // by default, polymesh Meshes are rendered:
    // - with gv::polygons if mesh.faces().size() > 0
    // - otherwise with gv::lines if mesh.edges().size() > 0
    // - otherwise with gv::points
    // this can be overridden:
    {
        auto g = gv::grid();

        gv::view(gv::polygons(pos), "gv::polygons(pos)");
        gv::view(gv::lines(pos), "gv::lines(pos)");
        gv::view(gv::points(pos), "gv::points(pos)");
    }

    // rendering can be customized by a builder-pattern:
    {
        // gv::polygons assumes planar faces and creates a triangle fan per face
        // normals used for shading can be customized

        auto g = gv::grid();

        gv::view(gv::polygons(pos).face_normals(), "face normals (default)");
        gv::view(gv::polygons(pos).smooth_normals(), "smooth normals");

        const pm::vertex_attribute<tg::vec3> vnormals = pm::vertex_normals_by_area(pos);
        gv::view(gv::polygons(pos).normals(vnormals), "custom normals");

        // see PolygonBuilder.hh for more information
    }
    {
        // gv::lines performs line rendering
        // by default, lines are rendered as 3D capsule with a fixed screen-space size
        // customization includes
        //   - billboard mode (camera-facing or oriented by normals)
        //   - line width (screen space or world space) measures the whole line, i.e. diameter and not radius, which differs from point_size
        //   - cap mode (round, square, none)
        //   - extrapolation (on/off) of the color gradient between vertices onto the caps
        // configurations can be combined in many ways, warnings are printed on the console if something is not compatible

        auto g = gv::grid();

        const pm::vertex_attribute<tg::vec3> vnormals = pm::vertex_normals_by_area(pos);
        const pm::edge_attribute<float> line_widths
            = m.edges().map([&](pm::edge_handle e) { return 0.25f * distance(pos(e.vertexA()), pos(e.vertexB())); });

        gv::view(gv::lines(pos).line_width_world(0.01f).normals(vnormals), "oriented billboard lines");
        gv::view(gv::lines(pos).line_width_px(10), "10px line width");
        gv::view(gv::lines(pos).line_width_world(0.01f), "1cm line width");
        gv::view(gv::lines(pos).line_width_world(line_widths), "per-line width");

        // see LineBuilder.hh for more information
    }
    {
        // build some planar connected random lines
        std::vector<tg::segment3> lines;
        const auto bb = tg::aabb3({-1, 0, -1}, {1, 0, 1});
        auto p = uniform(rng, bb);
        for (auto i = 0; i < 30; ++i)
        {
            auto q = uniform(rng, bb);
            lines.emplace_back(p, q);
            p = q;
        }

        // billboard lines (oriented by a normal) can have different cap types
        auto g = gv::grid();
        gv::view(gv::lines(lines).line_width_world(0.04f).normals(tg::vec3::unit_y).round_caps(), "round caps");
        gv::view(gv::lines(lines).line_width_world(0.04f).normals(tg::vec3::unit_y).square_caps(), "square caps");
        gv::view(gv::lines(lines).line_width_world(0.04f), "3D lines always have round caps");
        gv::view(gv::lines(lines).line_width_world(0.04f).normals(tg::vec3::unit_y).no_caps(), "no caps");
    }
    {
        auto g = gv::grid();

        const pm::vertex_attribute<tg::vec3> vnormals = pm::vertex_normals_by_area(pos);

        // compute per-vertex average edge length as point size
        pm::edge_attribute<float> edge_lengths = m.edges().map([&](pm::edge_handle e) { return edge_length(e, pos); });
        const pm::vertex_attribute<float> ptsize = m.vertices().map([&](pm::vertex_handle v) { return 0.5f * v.edges().avg(edge_lengths); });

        gv::view(gv::points(pos).point_size_px(10), "10px screen-size spheres");
        gv::view(gv::points(pos).point_size_world(0.03f).spheres(), "3D world space spheres"); // The default is 3D spheres, calling .spheres() is not necessary
        gv::view(gv::points(pos).camera_facing().square(), "camera facing billboard squares"); // The default is camera facing, calling .camera_facing() is not necessary
        gv::view(gv::points(pos).point_size_world(ptsize).normals(vnormals).round(), "normal oriented disks with adaptive point size");
        // note: point sizes measure the radius of the points, which differs from line_width

        // see PointBuilder.hh for more information
    }

    // gv::polygons, gv::lines, gv::points can also be used to customize other inputs
    // see typed_geometry_objects() example

    // TODO: text
    // TODO: images
}

void imguizmo(pm::vertex_attribute<tg::pos3> const& pos)
{
    // This sample shows how to use ImGuizmo, a very useful addon for ImGui for editing 4x4 affine transformation matrices using the viewer
    // For additional information refer to https://github.com/CedricGuillemet/ImGuizmo/

    // your own camera is required to access the camera's view and projection matrix
    auto cam = gv::CameraController::create();

    // cache the renderable
    auto r = gv::make_renderable(pos);

    // local vs global coordinate system
    auto currentGizmoMode = ImGuizmo::LOCAL;

    // the operation that we want to perform (rotate, scale, or translate)
    auto currentGizmoOperation = ImGuizmo::ROTATE;

    // do we want to snap to specific values
    bool useSnap = false;

    // the values that we want to snap to, i.e. 1 degree steps
    float snap[3] = {1.f, 1.f, 1.f};

    // the transformation that we want to edit
    auto transform = tg::mat4::identity;

    // an interactive view is required
    gv::interactive([&](float) {
        // you can define keybindings to select an operation
        if (ImGui::IsKeyPressed(GLFW_KEY_T))
            currentGizmoOperation = ImGuizmo::TRANSLATE;
        if (ImGui::IsKeyPressed(GLFW_KEY_R))
            currentGizmoOperation = ImGuizmo::ROTATE;
        if (ImGui::IsKeyPressed(GLFW_KEY_S))
            currentGizmoOperation = ImGuizmo::SCALE;

        // you can also choose the correct operation using gui elements
        if (ImGui::RadioButton("Translate", currentGizmoOperation == ImGuizmo::TRANSLATE))
            currentGizmoOperation = ImGuizmo::TRANSLATE;
        ImGui::SameLine();
        if (ImGui::RadioButton("Rotate", currentGizmoOperation == ImGuizmo::ROTATE))
            currentGizmoOperation = ImGuizmo::ROTATE;
        ImGui::SameLine();
        if (ImGui::RadioButton("Scale", currentGizmoOperation == ImGuizmo::SCALE))
            currentGizmoOperation = ImGuizmo::SCALE;

        // ImGuizmo decomposes your input transform into translation, rotation, and scaling components
        float matrixTranslation[3], matrixRotation[3], matrixScale[3];
        ImGuizmo::DecomposeMatrixToComponents(&transform[0][0], matrixTranslation, matrixRotation, matrixScale);

        // show the three components individually
        ImGui::InputFloat3("Tr", matrixTranslation);
        ImGui::InputFloat3("Rt", matrixRotation);
        ImGui::InputFloat3("Sc", matrixScale);

        // rebuild your 4x4 transform matrix
        ImGuizmo::RecomposeMatrixFromComponents(matrixTranslation, matrixRotation, matrixScale, &transform[0][0]);

        // only show the radio button to choose local/global coordinate system when NOT scaling
        // as scaling is always in local coordinates
        if (currentGizmoOperation != ImGuizmo::SCALE)
        {
            if (ImGui::RadioButton("Local", currentGizmoMode == ImGuizmo::LOCAL))
                currentGizmoMode = ImGuizmo::LOCAL;
            ImGui::SameLine();
            if (ImGui::RadioButton("World", currentGizmoMode == ImGuizmo::WORLD))
                currentGizmoMode = ImGuizmo::WORLD;
        }

        // toggle snap on p key
        if (ImGui::IsKeyPressed(GLFW_KEY_P))
            useSnap = !useSnap;

        // ... or use gui elements
        ImGui::Checkbox("", &useSnap);
        ImGui::SameLine();

        // name the snap input accordingly
        switch (currentGizmoOperation)
        {
        case ImGuizmo::TRANSLATE:
            ImGui::InputFloat3("Snap", snap);
            break;
        case ImGuizmo::ROTATE:
            ImGui::InputFloat("Angle Snap", snap);
            break;
        case ImGuizmo::SCALE:
            ImGui::InputFloat("Scale Snap", snap);
            break;
        default:   // silence warnings as there are quite a few more possible operations supported by ImGuizmo
            break; // do nothing
        }

        // ImGuizmo requires view and projection matrix for rendering.
        // However, it does not work with a reversed z projection matrix, which is used by the viewer by default
        // So we need to disable it temporarily, generate a projection matrix without it, and then reactivate it
        auto view = cam->computeViewMatrix();
        auto const rev_z_enabled = cam->reverseZEnabled();
        cam->setReverseZEnabled(false); //
        auto project = cam->computeProjMatrix();
        cam->setReverseZEnabled(rev_z_enabled);

        // This is where all the ImGuizmo magic comes together
        ImGuizmo::Manipulate(&view[0][0], &project[0][0], currentGizmoOperation, currentGizmoMode, &transform[0][0], nullptr, useSnap ? &snap[0] : nullptr);

        // Because the view is resized when we change the object, it might be a good idea to pass the viewer a fixed bounding box
        auto const aabb = tg::aabb3(tg::pos3(-2), tg::pos3(2));
        gv::view(r, cam, transform, aabb);
    });
}

void advanced_visualization(pm::vertex_attribute<tg::pos3> const& pos)
{
    // the args in gv::view(obj, args) do not only configure the viewer but also change how the renderable is visualized

    pm::Mesh const& m = pos.mesh();

    // RNG for creating some random sample values
    tg::rng rng;

    // colors
    {
        auto g = gv::grid();

        // generate some random color attributes
        pm::face_attribute<tg::color3> random_face_colors = m.faces().map([&](pm::face_handle) { return tg::uniform<tg::color3>(rng); });
        pm::vertex_attribute<tg::color3> random_vertex_colors = m.vertices().map([&](pm::vertex_handle) { return tg::uniform<tg::color3>(rng); });
        pm::edge_attribute<tg::color3> random_edge_colors = m.edges().map([&](pm::edge_handle) { return tg::uniform<tg::color3>(rng); });
        pm::halfedge_attribute<tg::color3> random_halfedge_colors = m.halfedges().map([&](pm::halfedge_handle) { return tg::uniform<tg::color3>(rng); });

        // map positions to colors
        pm::vertex_attribute<tg::color3> vcolors = m.vertices().map([&](pm::vertex_handle v) { return tg::color3(pos[v] * 0.5f + 0.5f); });

        // Normals are needed for two colored lines
        pm::vertex_attribute<tg::vec3> vnormals = pm::vertex_normals_by_area(pos);

        gv::view(pos, tg::color3::red, "solid color for whole mesh");
        gv::view(pos, random_face_colors, "random face colors");
        gv::view(pos, random_vertex_colors, "random vertex colors");
        gv::view(pos, random_halfedge_colors, "random halfedge colors");
        gv::view(gv::points(pos), vcolors, "point cloud with xyz colors");
        gv::view(gv::points(pos), random_vertex_colors, gv::no_shading, "unlit points");
        gv::view(gv::lines(pos).normals(vnormals).force3D(), random_face_colors, "two colored lines with random face colors");
        gv::view(gv::lines(pos), random_vertex_colors, "lines with random vertex colors");
        gv::view(gv::lines(pos), random_edge_colors, "lines with random edge colors");
        gv::view(gv::lines(pos), random_edge_colors, gv::no_shading, "unlit lines");
        // note: when normals are provided, line rendering defaults to normal aligned flat lines.
        // use force3D() to render these lines as 3D capsules anyway.
        // without normals, the default rendering is 3D capsules, so force3D is not necessary there.
        // The default values of gv::lines can be seen and changed in LineRenderable.cc LineRenderable::initFromBuilder (respectively PointRenderable for gv::points)
    }

    // data mapping
    {
        auto g = gv::grid();

        // a scalar field consisting of vertex y coordinate
        pm::vertex_attribute<float> vdata = pos.map([](tg::pos3 v) { return v.y; });
        // a scalar field consisting of face areas
        pm::face_attribute<float> fdata = m.faces().map([&](pm::face_handle f) { return pm::face_area(f, pos); });

        // map vertex data to black-red, from 0.1 .. 0.3 (repeats outside)
        gv::view(pos, gv::mapping(vdata).linear(tg::color3::black, tg::color3::red, 0.1f, 0.3f), "vertex data");

        // map face data to red (small) to green (big) (data outside the range is clamped)
        gv::view(pos, gv::mapping(fdata).linear(tg::color3::red, tg::color3::green, 0.00001f, 0.001f).clamped(), "face data");
    }

    // masking
    {
        auto g = gv::grid();

        // two cuts along the same plane, one smooth the other along primitive edges
        // note that they are the same because the first is compared against the default threshold of 0.5 and the second is converted to boolean
        auto smooth_cut_mask = pm::vertex_attribute<float>(pos.map([](tg::pos3 v) { return v.x + 0.3f; }));
        auto polygon_cut_mask = pm::vertex_attribute<bool>(pos.map([](tg::pos3 v) { return v.x > 0.2f; }));

        // some random boolean masks
        pm::face_attribute<bool> random_face_mask = m.faces().map([&](pm::face_handle) { return tg::uniform(rng, {true, false}); });
        pm::vertex_attribute<bool> random_vertex_mask = m.vertices().map([&](pm::vertex_handle) { return tg::uniform(rng, {true, false}); });

        // some scalar valued data that varies smoothly
        pm::vertex_attribute<float> vdata = m.vertices().map([&](pm::vertex_handle v) { return tg::cos(720_deg * (pos[v].x + pos[v].y + pos[v].z)); });

        // with gv::masked(...) it is possible to discard geometry from being rendered
        // (this is done on a per-pixel basis, that interpolates between the values at the vertices)
        // Note that the default threshold is 0.5, since that works better with the conversion of false to 0 and true to 1
        gv::view(pos, gv::masked(smooth_cut_mask), "smooth cut mask");
        gv::view(pos, gv::masked(random_face_mask), "randomly masked-out faces");
        gv::view(pos, gv::masked(random_vertex_mask), "randomly masked-out vertices");
        gv::view(pos, gv::masked(polygon_cut_mask), "cut along primitive edges");
        gv::view(pos, gv::masked(vdata, 0.2f), "threshold-based mask");
        gv::view(gv::lines(pos), gv::masked(vdata, 0.2f), "also works for lines"); // and points as well
    }

    // textures
    {
        auto g = gv::grid();

        // taking xy as UV for now
        // NOTE: halfedge attributes also work
        pm::vertex_attribute<tg::pos2> uv = pos.map([](tg::pos3 v) { return tg::pos2(v.x, v.y); });

        // textures can be loaded directly or async
        // TODO: gv::textured with a filename (and color space)
        glow::SharedTexture2D tex = glow::Texture2D::createFromFile(dataPath + "textures/tiles.color.png", glow::ColorSpace::sRGB);
        glow::AsyncTexture2D atex = glow::AsyncTextureLoader::load2D(dataPath + "textures/tiles.color.png", glow::ColorSpace::sRGB);

        gv::view(pos, gv::textured(uv, tex), "textured");
        gv::view(pos, gv::textured(uv, atex), "textured (async)");
        gv::view(pos, gv::textured(uv, tex).flip(), "flipped y axis");
        gv::view(pos, gv::textured(uv, tex).transform(tg::scaling(3.0f, 0.5f)), "transformed UVs");

        // gv::envmap can be used to set a reflective cubemap
        {
            auto pbt = dataPath + "cubemap/miramar";
            auto envmap = glow::TextureCubeMap::createFromData(glow::TextureData::createFromFileCube( //
                pbt + "/posx.jpg",                                                                    //
                pbt + "/negx.jpg",                                                                    //
                pbt + "/posy.jpg",                                                                    //
                pbt + "/negy.jpg",                                                                    //
                pbt + "/posz.jpg",                                                                    //
                pbt + "/negz.jpg",                                                                    //
                glow::ColorSpace::sRGB));

            gv::view(gv::polygons(pos).smooth_normals(), tg::color3(0.2f), gv::envmap(envmap, 0.7f), "env map");
        }
    }

    // transparency
    {
        auto g = gv::grid();

        // if colors with alpha < 1 are provided, transparency is enabled
        gv::view(pos, tg::color4(0, 0.4f, 0.3f, 0.2f), "transparency with fresnel");
        gv::view(pos, tg::color4(0, 0.4f, 0.3f, 0.2f), gv::no_fresnel, "transparency without fresnel");

        // it can be explicitly controlled via gv::transparent and gv::opaque
        gv::view(pos, tg::color4(0.3f, 0.1f, 0.3f, 0.5f), gv::transparent, "explicitly transparent");
        gv::view(pos, tg::color4(0.3f, 0.1f, 0.3f, 0.2f), gv::opaque, "transparent color but disabled transparency");
    }

    // TODO: complex material (e.g. PBR)
}

void typed_geometry_objects()
{
    auto g = gv::grid();

    // RNG for creating some random sample values
    tg::rng rng;

    // TODO: support 2D types
    // TODO: support ranges of tg types
    // TODO: support integer and double versions
    // TODO: support per-individual colored versions
    // TODO: support more tg objects

    // 3D typed geometry objects can be rendered directly
    gv::view(gv::points(tg::pos3::zero).point_size_world(0.25f), "tg::pos"); // point_size_world is only used to give the grid on the ground a reference scale
    gv::view(tg::segment3(tg::pos3(0, 0, 0), tg::pos3(1, 0, 0)), tg::color3::red, "tg::segment");
    gv::view(tg::triangle3({0, 0, 0}, {1, 0, 0}, {0, 1, 0}), tg::color3::blue, "tg::triangle");
    gv::view(tg::aabb3({-0.3f, 0, -0.4f}, {0.2f, 0.5f, 0.1f}), tg::color3::green, "tg::aabb");

    // gv::lines can be used to make them into line drawings instead of solids
    gv::view(gv::lines(tg::aabb3({-0.3f, 0, -0.4f}, {0.2f, 0.5f, 0.1f})), "lines(tg::aabb)");

    // vector versions
    {
        std::vector<tg::pos3> pts;
        pts.reserve(500);
        for (auto i = 0; i < 500; ++i)
            pts.push_back(uniform(rng, tg::sphere3::unit));
        gv::view(pts, "point cloud");
    }
    {
        std::vector<tg::segment3> segs;
        segs.reserve(20);
        for (auto i = 0; i < 20; ++i)
            segs.emplace_back(uniform(rng, tg::sphere3::unit), uniform(rng, tg::sphere3::unit));
        gv::view(segs, "tg::segments");
    }
    {
        std::vector<tg::triangle3> tris;
        tris.reserve(20);
        for (auto i = 0; i < 20; ++i)
            tris.emplace_back(uniform(rng, tg::sphere3::unit), uniform(rng, tg::sphere3::unit), uniform(rng, tg::sphere3::unit));
        gv::view(tris, "tg::triangle soup");
    }
    {
        std::vector<tg::quad3> quads;
        quads.reserve(20);
        for (auto i = 0; i < 20; ++i)
        {
            auto p = uniform(rng, tg::sphere3::unit);
            auto t = uniform_vec(rng, tg::sphere3::unit);
            auto b = uniform_vec(rng, tg::sphere3::unit);
            quads.emplace_back(p - t - b, //
                               p - t + b, //
                               p + t + b, //
                               p + t - b);
        }
        gv::view(quads, "tg::quad soup");
    }
    {
        std::vector<tg::box3> bbs;
        for (auto i = 0; i < 4; ++i)
        {
            auto e0 = uniform_vec(rng, tg::sphere3::unit);
            auto e1 = cross(e0, uniform_vec(rng, tg::sphere3::unit));
            auto e2 = cross(e1, e0);

            const auto c = uniform(rng, tg::sphere3::unit);

            e0 = normalize(e0) * uniform(rng, 0.2f, 0.5f);
            e1 = normalize(e1) * uniform(rng, 0.2f, 0.5f);
            e2 = normalize(e2) * uniform(rng, 0.2f, 0.5f);
            bbs.push_back(tg::box3(c, {e0, e1, e2}));
        }

        gv::view(bbs, "tg::box soup");
        gv::view(gv::lines(bbs), "tg::box frames");
    }
}

void advanced_configs(pm::vertex_attribute<tg::pos3> const& pos)
{
    // with this macro, all configurations are added to ALL views created until the end of the scope
    GV_SCOPED_CONFIG(gv::dark_ui);

    auto g = gv::grid();

    // graphics settings
    gv::view(pos, gv::no_shading, "no shading");
    gv::view(pos, gv::no_grid, "no grid");
    gv::view(pos, gv::no_shadow, "no shadow");
    gv::view(pos, gv::no_outline, "no outline");
    gv::view(pos, gv::no_ssao, "no ssao");
    gv::view(pos, gv::clip_plane(tg::pos3(0, .5f, 0), tg::vec3(1, 1, 1)), "clipping plane");
    gv::view(pos, gv::ssao_power(1.0f), "weaker SSAO");
    gv::view(pos, gv::ssao_radius(0.2f), "smaller SSAO");
    gv::view(pos, gv::tonemap_exposure(1.5f), "tonemapping");
    gv::view(pos, tg::aabb3(-2, 2), "custom scene AABB");
    gv::view(pos, gv::grid_size(0.3f), gv::grid_center({1, 2, 3}), "custom grid configuration");
    gv::view(pos, gv::shadow_screen_fadeout_distance(100.f), gv::print_mode, gv::no_grid, gv::total_shadow_samples(4096), "100px shadow fadeout");

    gv::view(pos, gv::print_mode, "print-friendly mode");
    gv::view(pos, gv::background_color(tg::color3::blue), "custom BG color");
    // note: what may look like a vignette effect is the default background containing a gradient.
    // it can be disabled by settings a constant background_color.

    // some configurations have booleans that can be passed
    gv::view(pos, gv::print_mode(false), "disabled print mode");

    // explicit view configure
    {
        auto v = gv::view(pos, "explicit view configure");
        v.configure(gv::no_grid, gv::no_outline);
    }

    // custom configure function
    gv::view(pos, "config via lambda", [](gv::SceneConfig& cfg) {
        cfg.enableShadows = false;
        cfg.bgColorInner = {1, 0, 1};
    });
}

void advanced_layouting(pm::vertex_attribute<tg::pos3> const& pos)
{
    // gv::grid() automatically layouts subviews in a grid pattern
    // trying to preserve a 1:1 aspect ratio
    // gv::grid(1.6f) would try a 16 : 10 ratio
    // gv::grid(3, 4) would make 3 columns with 4 rows
    auto g = gv::grid();
    gv::view(pos, "nested grid layout");
    gv::view(pos);
    {
        // a column sub layout
        auto c = gv::columns();
        gv::view(pos, " with columns");
        gv::view(pos);
        gv::view(pos);
    }
    {
        // a row sub layout
        auto c = gv::rows();
        gv::view(pos);
        gv::view(pos);
        gv::view(pos, "and rows");
    }
}

void viewer_canvas(pm::vertex_attribute<tg::pos3> const& pos)
{
    // auto c = gv::canvas();
    // this creates a canvas where large amount of primitives can be added without performance penalties
    // the canvas itself behaves like a view and can be nested

    auto g = gv::grid();

    // c.add_points allows anything vec3-like and tg objects that defines vertices(obj) or ranges thereof
    // c.add_point is the single point only version of that
    {
        auto c = gv::canvas();

        tg::rng rng;
        for (auto i = 0; i < 50; ++i)
            c.add_point(uniform(rng, tg::aabb3(-10, 10)));

        // types do not have to match exactly (e.g. ipos3 also works)
        for (auto i = 0; i < 50; ++i)
            c.add_point(uniform(rng, tg::iaabb3(-10, 10)));

        // color can be set in a sticky way
        c.set_color(tg::color3::red);
        for (auto i = 0; i < 100; ++i)
            c.add_points(uniform(rng, tg::aabb3(-10, 10)));

        // .. same as point size (in world or px)
        c.set_point_size_world(0.5f);
        c.set_color(tg::color3::blue);
        for (auto i = 0; i < 20; ++i)
            c.add_points(uniform(rng, tg::aabb3(-10, 10)));

        // colors can also be provided as second argument
        for (auto i = 0; i < 20; ++i)
            c.add_points(uniform(rng, tg::aabb3(-10, 10)), tg::uniform<tg::color3>(rng));

        // meshes work by rendering their vertices
        c.set_point_size_px();
        c.add_points(pos);

        // ranges can be used as well
        std::vector<tg::pos3> pts;
        for (auto i = 0; i < 20; ++i)
            pts.push_back(uniform(rng, tg::aabb3(-10, 10)));
        c.add_points(pts, tg::color3::yellow);

        // tg::objects that support vertices_of(obj) work
        c.add_points(tg::aabb3(-10, 10), tg::color3::green);
    }

    // c.add_splats is similar to points but has normals as well (rendered as world sized splats)
    // c.add_point is the single splat only version
    {
        auto c = gv::canvas();

        tg::rng rng;

        // splats are always world sized and have no default size
        c.set_splat_size(0.4f);

        for (auto i = 0; i < 50; ++i)
        {
            auto p = uniform(rng, tg::boundary_of(tg::sphere3(tg::pos3::zero, 8.f)));
            auto n = normalize(p - tg::pos3::zero);
            c.add_splat(p, n);
        }
    }

    // c.add_lines allows segments and tg objects that defines edges(obj) or ranges thereof
    // c.add_line is the single line only version
    {
        auto c = gv::canvas();

        // meshes work by rendering their edges
        c.add_lines(pos);

        // tg::objects that support edges_of(obj) work (and types do not have to be float)
        c.add_lines(tg::iaabb3(-10, 10), tg::color3::green);

        // add_line can be used with pos/pos or pos/vec as well
        tg::rng rng;
        for (auto i = 0; i < 10; ++i)
            c.add_line(tg::pos3(1, 2, 3), tg::uniform<tg::dir3>(rng) * uniform(rng, 0.3f, 0.8f), tg::color3::blue);
    }

    // c.add_faces allows triangles, quads, meshes, anything with a surface or ranges thereof
    // c.add_face is the single line only version
    {
        auto c = gv::canvas();

        // meshes work by rendering their faces (assumes planar polygons)
        c.add_faces(pos);

        // most finite tg::objects work
        c.add_faces(tg::aabb3(-3, -2), tg::color3::red);

        // add_face can be used with pos/pos/pos (triangle) or pos/pos/pos/pos (quad) as well
        tg::rng rng;
        for (auto i = 0; i < 10; ++i)
        {
            auto p0 = tg::pos3(1, 2, 3);
            auto p1 = p0 + tg::uniform<tg::dir3>(rng) * uniform(rng, 0.3f, 0.8f);
            auto p2 = p0 + tg::uniform<tg::dir3>(rng) * uniform(rng, 0.3f, 0.8f);
            c.add_face(p0, p1, p2, tg::color3::blue);
        }
    }

    // return value of add_xyz
    // is a reference to all added primitives
    // and can be used to change properties
    // NOTE: this is only valid until the next add_xyz call
    {
        auto c = gv::canvas();

        auto vnormals = pm::vertex_normals_by_area(pos);
        auto vcolors = pos.map([](tg::pos3 p) { return tg::color3(abs(p)); });

        // .color(...) changes the color of all added primitives in that call
        c.add_lines(tg::iaabb3(-8, 8)).color(0, 0, 1);

        // .size changes all sizes
        // .normal_translate translates each splat in normal direction
        // .colors can be used to set different points per splat
        c.add_splats(pos, vnormals).size(0.01f).normal_translate(0.03f).colors(vcolors);

        // .color(...) also works with hex strings
        c.add_faces(pos).color("#f00");
    }

    // extra features
    {
        auto c = gv::canvas();

        c.add_faces(pos);

        // .add_arrow can be used to add arrows
        tg::rng rng;
        for (auto i = 0; i < 20; ++i)
        {
            auto d = tg::uniform<tg::dir3>(rng);

            auto end = tg::pos3(d * 2.f);
            auto start = end + d * uniform(rng, 0.3f, 0.7f);

            c.add_arrow(start, end, 0.05f, tg::color3::red);
        }

        // .add_label can be used to add labels
        // normals is optional and can be used for preferred directions
        auto normals = pm::vertex_normals_by_area(pos);
        for (auto i = 0; i < 20; ++i)
        {
            auto v = pos.mesh().vertices().random(rng);
            c.add_points(pos[v], tg::color3::red);
            c.add_label(pos, normals, v, "vertex " + std::to_string(int(v)));
        }
    }
}

void interactive_viewer(pm::vertex_attribute<tg::pos3> const& pos)
{
    // gv::interactive([&](float dt) { ... });
    // this opens an interactive viewer
    // the lambda is called every frame
    // inside, viewer commands can be used, as well as ImGui UI elements
    // the scene config and all renderables are hashed and the viewer accumulation is cleared when the hash changes

    // a simple interactive viewer with some interactive-related controls
    gv::interactive([&](auto) {
        if (ImGui::Button("make screenshot"))
            gv::make_screenshot("screenshot.png", 1920, 1080);

        if (ImGui::Button("close viewer"))
            gv::close_viewer();

        gv::view(pos, "interactive viewer with custom buttons. It is slow because nothing is cached.");
    });

    // creating renderables is expensive, cache them whenever possible
    // NOTE: capture by value if the interactive viewer is not the top-most viewer
    {
        auto const r = gv::make_renderable(pos);
        gv::interactive([r](auto dt) {
            static auto time = 0.f;
            time += dt;

            gv::view(r, tg::translation(tg::vec3(tg::sin(tg::radians(time * .5f)) * .5f, 0.f, 0.f)), "Caching renderables in interactive views increases performance.");
        });
    }

    // using imgui in an interactive view
    {
        auto const r = gv::make_renderable(pos);
        gv::interactive([r](auto) {
            static float configurable = 0.f;

            ImGui::SliderFloat("Height", &configurable, -3.f, 3.f);

            gv::view(r, tg::translation(tg::vec3(0.f, configurable, 0.f)), "interactive translation of a model");
        });
    }

    // an interactive textured torus with animated texture coordinates
    {
        glow::SharedTexture2D tex = glow::Texture2D::createFromFile(dataPath + "textures/tiles.color.png", glow::ColorSpace::sRGB);

        // create torus mesh
        pm::Mesh m;
        auto pos = m.vertices().make_attribute<tg::pos3>();
        auto uv = m.vertices().make_attribute<tg::pos2>();
        pm::objects::add_quad(
            m,
            [&](pm::vertex_handle v, float x, float y) {
                auto [cx, sx] = tg::sin_cos(tg::pi<float> * 2 * x);
                auto [cy, sy] = tg::sin_cos(tg::pi<float> * 2 * y);

                auto const orad = 8.f;
                auto const irad = 3.f;

                tg::vec3 t;
                t.x = cx;
                t.z = sx;
                tg::pos3 p;
                p.x = orad * cx;
                p.y = irad * cy;
                p.z = orad * sx;
                p += t * irad * sy;

                pos[v] = p;
                uv[v] = {1 - x, y};
            },
            32, 32);

        auto a = 0.f;
        auto animate = false;
        gv::interactive([&](auto dt) {
            ImGui::Begin("Torus");
            ImGui::SliderFloat("angle", &a, 0.f, 360.f);
            ImGui::Checkbox("Animate", &animate);
            ImGui::End();

            if (animate)
                a += 5 * dt;

            // texture coordinates are rotated by "a" degrees
            view(pos, gv::textured(uv, tex).transform(tg::rotation_around(tg::pos2::zero, tg::degree(a))), "animated texture coordinates");
        });
    }
}

void scenarios()
{
    // in contrast to the other example groups, this group does not introduce new concepts.
    // it combines the single features and shows how they can be used in some scenarios

    // debugging / visualizing geometric operations
    {
        auto g = gv::grid();
        tg::rng rng;

        // construct a random box
        const auto c = uniform(rng, tg::sphere3::unit);
        auto e0 = uniform_vec(rng, tg::sphere3::unit);
        auto e1 = cross(e0, uniform_vec(rng, tg::sphere3::unit));
        auto e2 = cross(e1, e0);
        e0 = normalize(e0) * uniform(rng, 0.5f, 0.9f);
        e1 = normalize(e1) * uniform(rng, 0.5f, 0.9f);
        e2 = normalize(e2) * uniform(rng, 0.5f, 0.9f);
        const auto box = tg::box3(c, {e0, e1, e2});

        // check for some random points if they are inside or outside the box
        const auto range = tg::sphere3(tg::pos3::zero, 2.f);
        std::vector<tg::pos3> insidePoints;
        std::vector<tg::pos3> outsidePoints;
        for (auto i = 0; i < 1000; ++i)
        {
            const auto p = uniform(rng, range);
            if (contains(box, p))
                insidePoints.push_back(p);
            else
                outsidePoints.push_back(p);
        }

        {
            auto v = gv::view();
            gv::view(box, tg::color4(tg::color3::cyan, 0.5f), "Visualize contained and outside points");
            gv::view(gv::lines(box), tg::color3::cyan);
            gv::view(insidePoints, gv::maybe_empty, tg::color3::green);
            gv::view(outsidePoints, gv::maybe_empty, tg::color3::red);
        }

        // project outside points onto the box
        std::vector<tg::segment3> projections;
        projections.reserve(outsidePoints.size());
        for (const auto& p : outsidePoints)
            projections.emplace_back(p, project(p, box));

        {
            auto v = gv::view();
            gv::view(box, tg::color3::cyan, "Visualize projection of outside points");
            gv::view(outsidePoints, gv::maybe_empty);
            gv::view(projections, gv::maybe_empty);
        }

        // measure distance between points and box
        std::vector<tg::pos3> points;
        std::vector<float> distances;
        const auto num = 25;
        points.reserve(num);
        distances.reserve(num);
        for (auto i = 0; i < num; ++i)
        {
            auto p = uniform(rng, range);
            points.push_back(p);
            distances.push_back(distance(p, box));
        }

        {
            auto v = gv::view();
            gv::view(box, tg::color3::cyan, "Use point spheres to visualize distances");
            gv::view(points);
            gv::view(gv::points(points).point_size_world(distances)); // point_size specifies the radius of the rendered spheres
        }
    }
}

void vector_graphics()
{
    // TODO: text
    // TODO: 2D camera controls

    // see Vector2DSample for more sample code
    glow::vector::image2D img;
    auto g = graphics(img);
    g.fill(tg::disk2({100.f, 100.f}, 70), tg::color3::red);
    g.draw(tg::circle2({100.f, 100.f}, 70), {tg::color3::black, 2});
    gv::view(img, "2D vector graphics");
}

void custom_renderables()
{
    // TODO
}

void picking(pm::Mesh& m, pm::vertex_attribute<tg::pos3>& pos, pm::face_attribute<tg::color3> const& col)
{
    // gv::view(obj, args, gv::pick().on...([&](pm::face_index, tg::pos3, tg::vec3){...}), ...);
    // adds a Picker to a Renderable (MeshRenderable, PointRenderable, LineRenderable) that enables the picking of individual primitives and the visual representation
    // every time a primitive gets picked, the callback function defined in the gv::pick().on...() call will be executed
    // picking can be defined on up to two buttons and on hover (gv::pick().onLeftClick(...), gv::pick().onRightClick(...), gv::pick().onHover(...)).
    // callback functions need to have the signature as given above and might return a gv::view::picking_result or void.
    // gv::view::picking_result as return type can change the visual appearence of the picked primitive via modifying the member attributes pickingColor, borderColor, and borderWidth;
    //
    // in interactive mode (gv::interactive()) operations on picked primitives can be performed

    {
        auto g = gv::grid();

        // Only on_left_click callback defined
        gv::view(pos, col, gv::pick().onLeftClick([&](pm::face_index face_id, tg::pos3 world_pos, tg::vec3 normal) -> gv::picking_result {
            std::cout << "Something has been picked! ON_LEFT_CLICK"
                      << "ID: " << (int)face_id << std::endl;
            std::cout << "World_Position " << world_pos << std::endl;
            std::cout << "Normal " << normal << std::endl;
            gv::picking_result res;
            res.pickingColor = tg::color3::cyan;
            return res;
        }),
                 "picking MeshRenderable - callback on left click");

        // Only on_left_click callback defined - LineRenderable - uncolored
        gv::view(gv::lines(pos), gv::pick().onLeftClick([&](pm::face_index face_id, tg::pos3 world_pos, tg::vec3 normal) {
            std::cout << "Something has been picked! ON_LEFT_CLICK"
                      << "ID: " << int(face_id) << std::endl;
            std::cout << "World_Position" << world_pos << std::endl;
            std::cout << "Normal" << normal << std::endl;
        }),
                 "picking LineRenderable - callback on left click");

        {
            // Only on_left_click callback defined - PointRenderable - uncolored
            gv::view(gv::points(pos), gv::pick().onLeftClick([&](pm::vertex_index vertex_id, tg::pos3 world_pos, tg::vec3 normal) {
                std::cout << "Something has been picked! ON_LEFT_CLICK"
                          << "ID: " << int(vertex_id) << std::endl;
                std::cout << "World_Position" << world_pos << std::endl;
                std::cout << "Normal" << normal << std::endl;
                return;
            }),
                     "picking PointRenderable - callback on left click - spheres");
        }

        {
            // Only on_left_click callback defined - PointRenderable - uncolored - square billboards - NOT WORKING APPROPRIATELY
            gv::view(gv::points(pos).point_size_world(0.03f).camera_facing().square(),
                     gv::pick().onLeftClick([&](pm::vertex_index vertex_id, tg::pos3 world_pos, tg::vec3 normal) {
                         std::cout << "Something has been picked! ON_LEFT_CLICK"
                                   << "ID: " << int(vertex_id) << std::endl;
                         std::cout << "World_Position" << world_pos << std::endl;
                         std::cout << "Normal" << normal << std::endl;
                         return;
                     }),
                     "picking PointRenderables - callback on left click - billboards");
        }
    }

    {
        auto g = gv::grid();

        // Only on_right_click callback defined
        gv::view(pos, col, gv::pick().onRightClick([&](pm::face_index face_id, tg::pos3 world_pos, tg::vec3 normal) {
            std::cout << "Something has been picked! ON_RIGHT_CLICK"
                      << "ID: " << int(face_id) << std::endl;
            return;
        }),
                 "picking MeshRenderable - callback on right click");

        // Only on_hover callback defined
        gv::view(pos, col, gv::pick().onHover([&](pm::face_index face_id, tg::pos3 world_pos, tg::vec3 normal) {
            std::cout << "Something has been picked! ON_HOVER"
                      << "ID: " << (int)face_id << std::endl;
            return;
        }),
                 "picking MeshRenderable - callback on hover");
    }

    {
        /* Picking in interactive mode with ImGui example */
        pm::face_index face_index_i;
        uint32_t face_id_i;
        tg::pos3 world_pos_i;
        tg::vec3 normal_i;

        pm::Mesh m2;
        m2.copy_from(m);

        pm::vertex_attribute<tg::pos3> pos2(m2);
        pos2.copy_from(pos);

        gv::interactive([&](auto) {
            gv::view(pos2, col, gv::pick().onLeftClick([&](pm::face_index face_id, tg::pos3 world_pos, tg::vec3 normal) {
                std::cout << "Something has been picked! ON_LEFT_CLICK"
                          << "ID: " << int(face_id) << std::endl;
                std::cout << "World_Position" << world_pos << std::endl;
                std::cout << "Normal" << normal << std::endl;

                face_index_i = face_id;
                world_pos_i = world_pos;
                normal_i = normal;
            }),
                     "picking interactive mode - callback on left click - delete selected face");

            ImGui::Begin("Picking");

            ImGui::Value("pm::face_index", face_index_i.value);

            ImGui::Value("WorldPos.x", world_pos_i.x);
            ImGui::Value("WorldPos.y", world_pos_i.y);
            ImGui::Value("WorldPos.z", world_pos_i.z);

            ImGui::Value("Normal.x", normal_i.x);
            ImGui::Value("Normal.y", normal_i.y);
            ImGui::Value("Normal.z", normal_i.z);

            if (ImGui::Button("Delete selected face"))
            {
                // delete selected face - after deleting one face another face has to be picked
                std::cout << "DELETE FACE" << std::endl;
                m2.faces().remove(m2.faces()[face_index_i]);
            }

            ImGui::End();
        });
    }

    {
        /* Picking in interactive mode with ImGui example.*/
        pm::vertex_index vertex_index_i;
        uint32_t vertex_id_i;
        tg::pos3 world_pos_i;
        tg::vec3 normal_i;

        pm::Mesh m2;
        m2.copy_from(m);

        pm::vertex_attribute<tg::pos3> pos2(m2);
        pos2.copy_from(pos);

        gv::interactive([&](auto) {
            gv::view(gv::points(pos2),
                     gv::pick().onLeftClick([&vertex_index_i, &world_pos_i, &normal_i](pm::vertex_index vertex_id, tg::pos3 world_pos, tg::vec3 normal) {
                         std::cout << "Something has been picked! ON_LEFT_CLICK"
                                   << "ID: " << int(vertex_id) << std::endl;
                         std::cout << "World_Position" << world_pos << std::endl;
                         std::cout << "Normal" << normal << std::endl;

                         vertex_index_i = vertex_id;
                         world_pos_i = world_pos;
                         normal_i = normal;

                         return;
                     }),
                     "picking interactive mode - PointRenderable");

            ImGui::Begin("Picking");

            ImGui::Value("pm::vertex_index", vertex_index_i.value);

            ImGui::Value("WorldPos.x", world_pos_i.x);
            ImGui::Value("WorldPos.y", world_pos_i.y);
            ImGui::Value("WorldPos.z", world_pos_i.z);

            ImGui::Value("Normal.x", normal_i.x);
            ImGui::Value("Normal.y", normal_i.y);
            ImGui::Value("Normal.z", normal_i.z);

            if (ImGui::Button("Delete selected vertex"))
            {
                // delete selected vertex - after deleting one vertex another vertex has to be picked
                std::cout << "DELETE VERTEX" << std::endl;
                m2.vertices().remove(m2.vertices()[vertex_index_i]);
            }

            ImGui::End();
        });
    }

    {
        // On_hover, on_right_click, and on_left_click callbacks defined simultaneously
        gv::view(pos, col,
                 gv::pick()
                     .onLeftClick([&](pm::face_index face_id, tg::pos3 world_pos, tg::vec3 normal) {
                         std::cout << "Something has been picked! ON_LEFT_CLICK"
                                   << "ID: " << int(face_id) << std::endl;
                         return;
                     })
                     .onRightClick([&](pm::face_index face_id, tg::pos3 world_pos, tg::vec3 normal) {
                         std::cout << "Something has been picked! ON_RIGHT_CLICK"
                                   << "ID: " << int(face_id) << std::endl;
                         return;
                     })
                     .onHover([&](pm::face_index face_id, tg::pos3 world_pos, tg::vec3 normal) {
                         std::cout << "Something has been picked! ON_HOVER"
                                   << "ID: " << int(face_id) << std::endl;
                         return;
                     }),
                 "picking MeshRenderable - multiple callbacks: on left click, on right click, on hover");
    }

    {
        // User-defined Picking IDs - MeshRenderable
        pm::face_attribute<int32_t> fa = pm::face_attribute<int32_t>(m);
        int i = 0;
        for (auto f : m.faces())
        {
            fa[f] = 1000 + i;
            i++;
        }
        gv::view(pos, col, gv::pick(fa).onLeftClick([&](pm::face_index face_id, tg::pos3 world_pos, tg::vec3 normal) -> gv::picking_result {
            std::cout << "Something has been picked! ON_LEFT_CLICK"
                      << "ID: " << (int)face_id << std::endl;
            std::cout << "World_Position" << world_pos << std::endl;
            std::cout << "Normal" << normal << std::endl;
            gv::picking_result res;
            res.pickingColor = tg::color3::cyan;
            return res;
        }),
                 "simple picking: callback on left click - user-defined IDs");
    }

    {
        auto v = gv::view(pos, "pciking multiple renderables: different callbacks defined");

        // Check multiple Renderables.
        gv::view(pos, gv::pick().onHover([&](pm::face_index face_id, tg::pos3 world_pos, tg::vec3 normal) -> gv::picking_result {
            std::cout << "Something has been picked! PICKER 1 "
                      << "ID: " << (int)face_id << std::endl;
            std::cout << "World_Position" << world_pos << std::endl;
            std::cout << "Normal" << normal << std::endl;
            gv::picking_result res;
            res.pickingColor = tg::color3::cyan;
            return res;
        }));

        pm::Mesh m2;
        m2.copy_from(m);

        pm::vertex_attribute<tg::pos3> pos2(m2);
        pos2.copy_from(pos);

        for (auto p : m2.vertices())
        {
            pos2[p] = pos2[p] + tg::vec3(2, 2, 2);
        }

        gv::view(pos2, gv::pick().onLeftClick([&](pm::face_index face_id, tg::pos3 world_pos, tg::vec3 normal) -> gv::picking_result {
            std::cout << "Something has been picked! PICKER 2 "
                      << "ID: " << (int)face_id << std::endl;
            std::cout << "World_Position" << world_pos << std::endl;
            std::cout << "Normal" << normal << std::endl;
            gv::picking_result res;
            res.pickingColor = tg::color3::cyan;
            return res;
        }));
    }
}

void headless_screenshot(pm::vertex_attribute<tg::pos3> const& pos)
{
    // Viewer never shows a window, returns once screenshot is rendered
    GV_SCOPED_CONFIG(gv::headless_screenshot(tg::ivec2(1000, 1000), 32, "demo_screenshot.png"));
    gv::view(pos);
}

void cache_window_size(pm::vertex_attribute<tg::pos3> const& pos)
{
    // on closing the viewer, the window size and position are usually saved to and read from a
    // glfwapp.ini file (and imgui.ini) in the current working directory
    //
    // this behavior can be changed using gv::cache_window_size(folder) or gv::no_cache_window_size
    // -> gv::cache_window_size(folder) changes the folder for the .ini files
    //    - folder can be absolute (starting with "/") or relative to the CWD
    //    - the trailing "/" has no effect and is added automatically if it is not present
    //    - if the folder does not exist, a warning is emitted and the folder defaults to the CWD
    // -> gv::no_cache_window_size disables creating or reading these files (same as gv::cache_window_size(""))

    {
        // to avoid the creation of additional files, this is disabled in this sample file
        // auto cfg = gv::config(gv::cache_window_size("/my_folder_path/"));
        auto cfg = gv::config(gv::no_cache_window_size);
        gv::view(pos, "this window does not read or write .ini files for the window size");
    }

    {
        // interactive viewers currently depend on GV_SCOPED_CONFIG
        GV_SCOPED_CONFIG(gv::no_cache_window_size);
        gv::interactive([&](){
            gv::view(pos, "interactive version: this window does not read or write .ini files for the window size");
        });
    }
}

void special_use_cases(pm::vertex_attribute<tg::pos3> const& pos)
{
    // TODO: decoupled camera

    // extrapolation of attributes on line caps
    {
        pm::Mesh mLine;
        const auto v0 = mLine.vertices().add();
        const auto v1 = mLine.vertices().add();
        mLine.edges().add_or_get(v0, v1);
        const auto line = mLine.vertices().make_attribute_from_data<tg::pos3>({{0, 0, 0}, {1, 0, 0}});
        const auto lineColors = mLine.vertices().make_attribute_from_data<glow::colors::color>({{0.7f, 0.2f, 0.2f}, {0.2f, 0.7f, 0.2f}});

        auto v = glow::viewer::grid();
        gv::view(gv::lines(line).line_width_world(0.25f).extrapolate(), lineColors, "extrapolated color at line caps");
        gv::view(gv::lines(line).line_width_world(0.25f), lineColors, "whole line cap has the color of the vertex");
    }

    // empty renderable
    {
        auto g = gv::grid();
        const std::vector<tg::triangle3> tris;
        gv::view(tris, gv::maybe_empty, "allows empty renderable");
        gv::view(pos, gv::maybe_empty, "but does not necessarily have to be empty if specified");
    }

    gv::view(pos, gv::infinite_accumulation, "progressive rendering is not stopped early");

    // global settings (affect complete viewer)
    gv::view(pos, gv::dark_ui, "dark mode");
    gv::view(pos, gv::no_left_mouse_control, "disabled left mouse interaction");
    gv::view(pos, gv::no_right_mouse_control, "disabled right mouse interaction");
    {
        auto g = gv::grid();
        gv::view(pos, gv::subview_margin(5, tg::color3::red), "custom grid margin and color");
        gv::view(pos);
        gv::view(pos);
        gv::view(pos);
    }

    gv::view(pos, gv::camera_transform(tg::pos3(1, 1, 1), tg::pos3(0, 0, 0)), "explicit start position and target");
    gv::view(pos, gv::camera_orientation(125_deg, -15_deg, 1.7f), "explicit camera azimuth/altitude/distance");

    // custom close keys:
    // viewer closes when one of these keys are pressed
    // gv::get_last_close_info() returns:
    //   - which key closed the viewer
    //   - camera position and target when viewer was closed
    gv::view(pos, gv::close_keys('A', 'B', 'C'), "close the viewer by pressing A, B, or C");
    glow::info() << gv::get_last_close_info().closed_by_key;
    glow::info() << gv::get_last_close_info().cam_pos;
    glow::info() << gv::get_last_close_info().cam_target;

    // use an outer viewer object conditionally
    {
        const auto some_condition = false;
        auto v = some_condition ? gv::rows() : gv::nothing();

        // these two either nest into the outer object, or create their own windows
        gv::view(pos, "conditional grouping 1");
        gv::view(pos, "conditional grouping 2");
    }

    // a view can be explicitly shown before scope end
    // (it will not show again at the end of the scope)
    {
        auto v = gv::view(pos, "show viewer before end of scope");
        v.show();
    }

    // per default, cameras are shared for all views
    // by creating custom camera controllers, custom sharing can be configured
    {
        const auto camA = gv::CameraController::create();
        const auto camB = gv::CameraController::create();
        auto g = gv::grid();
        gv::view(pos, "built-in camera");
        gv::view(pos, "built-in camera");
        gv::view(pos, camA, "custom camera A");
        gv::view(tg::aabb3::unit_centered, camA, "custom camera A");
        gv::view(pos, camB, "custom camera B");
        gv::view(gv::lines(pos), camB, "custom camera B");
    }

    // per default, the camera is reset for each new view (and fit to the scene)
    // configuring gv::preserve_camera means the next view will use the same camera
    // configuring gv::reuse_camera means that the current view will use the last camera
    // these can be chained indefinitely
    {
        gv::view(pos, gv::preserve_camera, "next view will use the same camera");
        gv::view(pos);
        gv::view(pos, gv::reuse_camera, "re-used previous camera");
        gv::view(pos, gv::reuse_camera, "re-used previous camera (again)");
    }

    // most of the time, manually clearing the accumulated rendering is not necessary due to an internal hash system
    // however, sometimes it makes sense to manually clear the view (e.g. when dynamically changing texture content)
    {
        auto const r = gv::make_renderable(pos);

        gv::interactive([r](auto dt) {
            static auto time = 0.f;
            time += dt;

            // gv::view_cleared creates an always cleared view, resetting accumulation each frame
            gv::view_cleared(r, tg::translation(tg::vec3(tg::sin(tg::radians(time * .5f)) * .5f, 0.f, 0.f)));
        });

        gv::interactive([r](auto) {
            static float configurable = 0.f;

            auto changed = false;
            changed |= ImGui::SliderFloat("Height", &configurable, -3.f, 3.f);

            // gv::clear_accumulation conditionally clears the view accumulation
            gv::view(r, tg::translation(tg::vec3(0.f, configurable, 0.f)), gv::clear_accumulation(changed));
        });
    }
}

void subtle_cases(pm::vertex_attribute<tg::pos3> const& pos)
{
    // pos3-like includes doubles and integer
    gv::view(pos.to<tg::dpos3>(), "double positions");
    gv::view(pos.to<tg::ipos3>(), "integer positions");

    // doubles scalar attributes are converted to floats
    gv::view(gv::points(pos).point_size_world(0.01), "double uniform attributes");

    // double and integer transforms
    gv::view(pos, tg::scaling(2, 2, 2), "integer transform");
    gv::view(pos, tg::scaling(2.0, 2.0, 2.0), "double transform");
}

void known_issues(pm::vertex_attribute<tg::pos3> const&)
{
    pm::Mesh mLine;
    const auto v0 = mLine.vertices().add();
    const auto v1 = mLine.vertices().add();
    mLine.edges().add_or_get(v0, v1);
    const auto line = mLine.vertices().make_attribute_from_data<tg::pos3>({{0, 0, 0}, {1, 0, 0}});
    const auto lineColors = mLine.vertices().make_attribute_from_data<glow::colors::color>({{0.7f, 0.2f, 0.2f}, {0.2f, 0.7f, 0.2f}});

    // Intersection of screen space camera facing lines and viewer grid creates artifacts
    // Also the round caps of large screen space camera facing lines are not smoothly connecting to the straight line part
    {
        auto v = glow::viewer::view();
        gv::view(gv::lines(line).camera_facing().line_width_px(500.0f), tg::color3::red, "ground shadow intersects red screen space camera facing line");
        // It is fine for the green world space because the bounding box of world space moves the object up
        gv::view(gv::lines(line).camera_facing().line_width_world(0.25f), tg::color3::green, tg::translation(0.0f, 0.0f, 1.0f));
    }

    // Artifacts on grazing angles
    // When the camera is outside of the capsule but close to the line spanned by its axis, all sorts of artifacts occur
    {
        gv::view(gv::lines(line).line_width_px(250.0f), lineColors, "artifacts on grazing angles (screen space)");
        gv::view(gv::lines(line).line_width_world(1.5f), lineColors, tg::translation(0.f, 0.f, 5.f), "artifacts on grazing angles (screen space)");
        // Screen space camera facing lines seem fine
        // gv::view(gv::lines(line).line_width_px(250.0f).camera_facing(), lineColors, tg::translation(0.0f, 0.0f, -25.f));
    }
}

int main()
{
    // create a rendering context
    glow::glfw::GlfwContext ctx;

    // load a sample polymesh mesh
    pm::Mesh m;
    auto pos = m.vertices().make_attribute<tg::pos3>();
    load(dataPath + "suzanne.obj", m, pos);
    normalize(pos); // make it -1..1*/
    auto col = pm::face_attribute<tg::color3>(m);

    // initially random colors for faces (picking sample)
    auto r = tg::rng();
    for (auto x : m.faces())
    {
        tg::color3 color;
        color = tg::uniform<tg::color3>(r);
        col[x] = color;
    }

    // basic demos
    {
        simple_view(pos);

        basic_concepts(pos);
    }

    // intermediate demos
    {
        advanced_objects(pos);

        typed_geometry_objects();

        viewer_canvas(pos);

        advanced_visualization(pos);

        advanced_configs(pos);

        advanced_layouting(pos);

        interactive_viewer(pos);

        scenarios();
    }

    // expert / specific demos
    {
        vector_graphics();

        picking(m, pos, col);

        custom_renderables();

        special_use_cases(pos);

        subtle_cases(pos);

        headless_screenshot(pos);

        cache_window_size(pos);

        imguizmo(pos);
    }

    // known issues
    known_issues(pos);

    return EXIT_SUCCESS;
}