#version 330

in vec2 vPosition;

uniform samplerCube uTexture;

uniform mat4 uInvProj;
uniform mat4 uInvView;

out vec3 fColor;

void main() {
    //float d = distance(vPosition, vec2(0.5, 0.5));
    //d = d * d;
    //fColor = mix(vec3(.45), vec3(.2), smoothstep(0.0, 0.4, d));

    vec4 near = uInvProj * vec4(vPosition * 2 - 1, 0, 1);
    vec4 far = uInvProj * vec4(vPosition * 2 - 1, 0.5, 1);

    near /= near.w;
    far /= far.w;

    near = uInvView * near;
    far = uInvView * far;

    fColor = texture(uTexture, (far - near).xyz).rgb;
}
