#pragma once

#include <vector>

#include <glm/ext.hpp>

#include <glow/fwd.hh>
#include <glow/gl.hh>

#include <glow-extras/glfw/GlfwApp.hh>

class TextureMaterialSample : public glow::glfw::GlfwApp
{
public:
    TextureMaterialSample() : GlfwApp(Gui::AntTweakBar) {}

private:
    glow::SharedProgram mShader;
    glow::SharedProgram mShaderBG;
    glow::SharedProgram mShaderLight;
    glow::SharedVertexArray mCube;
    glow::SharedVertexArray mQuad;
    glow::SharedVertexArray mSphere;

    glow::SharedTextureCubeMap mCubeMap;

    glow::SharedTexture2D mTexAlbedo;
    glow::SharedTexture2D mTexAO;
    glow::SharedTexture2D mTexHeight;
    glow::SharedTexture2D mTexMetallic;
    glow::SharedTexture2D mTexNormal;
    glow::SharedTexture2D mTexRoughness;

    float mRuntime = 0.0f;
    bool mAnimate = false;

    glm::vec3 mLightDir = normalize(glm::vec3(-1, 5, 2));
    float mLightDis = 3.0f;
    float mLightRadius = 0.5f;

    bool mUseAlbedo = true;
    bool mUseAO = true;
    bool mUseHeight = true;
    bool mUseMetallic = true;
    bool mUseNormal = true;
    bool mUseRoughness = true;
    bool mUseReflection = true;

    int mShowTexture = -1;

protected:
    void init() override;
    void render(float elapsedSeconds) override;
};
