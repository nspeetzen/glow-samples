uniform sampler2DRect uTexture;

uniform float uAberrationStrength;

in vec2 vPosition;

out vec4 fColor;

void main() 
{
    vec4 color = texelFetch(uTexture, ivec2(gl_FragCoord.xy));

    float d = distance(vPosition, vec2(0.5)) * uAberrationStrength;
    vec2 dir = normalize(vPosition - vec2(0.5)) * d;

    fColor.r = texture(uTexture, gl_FragCoord.xy - dir * 1.0).r;
    fColor.g = texture(uTexture, gl_FragCoord.xy - dir * 0.8).g;
    fColor.b = texture(uTexture, gl_FragCoord.xy - dir * 0.7).b;
    fColor.a = 1.0;
}
