#include "DistanceFieldSample.hh"

#include <algorithm>
#include <fstream>

#include <glow/objects/ArrayBuffer.hh>
#include <glow/objects/AtomicCounterBuffer.hh>
#include <glow/objects/OcclusionQuery.hh>
#include <glow/objects/PrimitiveQuery.hh>
#include <glow/objects/Program.hh>
#include <glow/objects/Shader.hh>
#include <glow/objects/ShaderStorageBuffer.hh>
#include <glow/objects/Texture3D.hh>
#include <glow/objects/TimerQuery.hh>
#include <glow/objects/Timestamp.hh>
#include <glow/objects/TransformFeedback.hh>
#include <glow/objects/VertexArray.hh>

#include <glow/common/scoped_gl.hh>
#include <glow/common/str_utils.hh>

#include <glow-extras/assimp/Importer.hh>
#include <glow-extras/debugging/DebugRenderer.hh>
#include <glow-extras/geometry/Cube.hh>
#include <glow-extras/geometry/Quad.hh>
#include <glow-extras/pipeline/stages/StageCamera.hh>

#include <AntTweakBar.h>

#include <GLFW/glfw3.h>

#include "shader.hh"

#include "df.hh"

using namespace glow;

/**
 * Refs:
 *   - https://en.wikipedia.org/wiki/Distance_transform
 *   - https://en.wikipedia.org/wiki/Fast_marching_method
 *   - https://en.wikipedia.org/wiki/Fast_sweeping_method
 *   - https://github.com/thinks/fast-marching-method ?
 *
 * TODO:
 *   - Memory-minimal version
 *   - Seed-compression ("compactify")
 *   - Comparison with CPU
 *   - Tests
 *   - Quantify Error
 *   - Evaluate Moore vs von Neumann Neighborhood
 *   - k-NN
 *   - Two-hierarchy memory
 *   - Crust-only ("max distance")
 *   - Updating distance field
 *   - Eikonal with pseudo sorting
 *   - On Mesh
 *   - Exact (keep cells)
 *   - Check error for wrong voronoi cells (init vs iteration error)
 *   - Global transformation
 *
 * Subalgos 3D:
 *   - UDF PointCloud
 *   - UDF SplatCloud
 *   - UDF TriangleSoup
 *   - SDF Watertight TriangleMesh
 *   - UDF/SDF Mask Volume
 *
 * Subalgos 2D:
 *   - UDF PointCloud 2D
 *   - UDF SplatCloud 2D
 *   - UDF LineSoup
 *   - SDF Watertight Lines/Contours
 *   - UDF/SDF Mask Texture
 */

static void TW_CALL BtnCompute(void* clientData)
{
    ((DistanceFieldSample*)clientData)->recompute();
}

static bool ptCmp(glm::vec4 const& p1, glm::vec4 const& p2)
{
    if (p1.x != p2.x)
        return p1.x < p2.x;
    if (p1.y != p2.y)
        return p1.y < p2.y;
    return p1.z < p2.z;
}

void DistanceFieldSample::init()
{
    setUsePipeline(true);
    setGui(GlfwApp::Gui::AntTweakBar);

    // IMPORTANT: call base function
    glfw::GlfwApp::init();


    mGpuTimerStart = Timestamp::create();
    mGpuTimerEnd = Timestamp::create();
    mGpuTimer = TimerQuery::create();

    // bunny
    {
        std::ifstream file(util::pathOf(__FILE__) + "/../../../data/bunny.obj");

        glm::vec3 amin{std::numeric_limits<float>::max()};
        glm::vec3 amax{-std::numeric_limits<float>::max()};
        std::string line;
        std::vector<glm::vec3> pts;
        while (std::getline(file, line))
        {
            std::istringstream iss(line);
            std::string tag;
            iss >> tag;
            if (tag == "v")
            {
                float x, y, z;
                iss >> x >> y >> z;
                glm::vec3 v = {x, y, z};
                amin = min(amin, v);
                amax = max(amax, v);
                pts.push_back(v);
            }
        }

        auto s = amax - amin;
        amin -= s * 0.1f;
        amin += s * 0.1f;
        auto ms = glm::max(s.x, glm::max(s.y, s.z));
        for (auto v : pts)
        {
            v = (v - amin) / ms * Size;
            v.x += (ms - s.x) / 2.0f;
            v.y += (ms - s.y) / 2.0f;
            v.z += (ms - s.z) / 2.0f;
            mPoints.push_back(glm::vec4(v, 1.0f));
        }
    }

    // data
    if (false)
    {
        // mPoints.push_back(glm::vec4(Size / 2.0f));
        for (auto i = 0; i < 1000; ++i) // DEBUG
        {
            float r = Size * (0.7f + rand() / (float)RAND_MAX * 0.1f) / 2.0f;
            float x, y, z;
            do
            {
                x = rand() / (float)RAND_MAX * 2 - 1;
                y = rand() / (float)RAND_MAX * 2 - 1;
                z = rand() / (float)RAND_MAX * 2 - 1;
            } while (x * x + y * y + z * z > 1);
            mPoints.push_back(glm::vec4(normalize(glm::vec3(x, y, z)) * r + Size / 2.0f, 1.0f));
            // mPoints.push_back(glm::vec4(rand() % Size, rand() % Size, rand() % Size, 1.0));
        }
        for (auto i = 0; i < 1 * 1000; ++i) // DEBUG
            mPoints.push_back(glm::vec4(rand() % Size, rand() % Size, rand() % Size, 1.0));
    }

    if (false)
        std::sort(std::begin(mPoints), std::end(mPoints), ptCmp);
    // mPoints.push_back(glm::vec4(i % Size, (i / Size) % Size, (i / Size / Size) % Size, 1.0f) + 0.1f);
    mPointAB = ArrayBuffer::create();
    mPointAB->defineAttribute<glm::vec4>("aPosition");
    mPointAB->bind().setData(mPoints);
    mPointVAO = VertexArray::create(mPointAB, nullptr, GL_POINTS);
    mPointBuffer = ShaderStorageBuffer::createAliased(mPointAB);
    mTexDistanceField = Texture3D::createStorageImmutable(Size, Size, Size, GL_R32UI, 1);
    mTexNearestPoint = Texture3D::createStorageImmutable(Size, Size, Size, GL_R32UI, 1);

    for (auto const& tex : {mTexDistanceField, mTexNearestPoint})
    {
        auto t = tex->bind();
        t.setMinFilter(GL_NEAREST);
        t.setMagFilter(GL_NEAREST);
    }

    // rendering
    mSliceShader = Program::createFromFile(util::pathOf(__FILE__) + "/slice-shader");
    mQuadVAO = geometry::Quad<>().generate();

    // marching
    auto maxQueueSize = Size * Size * Size;
    mClearShader = Program::createFromFile(util::pathOf(__FILE__) + "/clear-shader");
    mInitShader = Program::createFromFile(util::pathOf(__FILE__) + "/init-shader");
    mMarchShader = Program::createFromFile(util::pathOf(__FILE__) + "/march-shader");
    mMarchShader->setShaderStorageBuffer("bPoints", mPointBuffer);
    mFeedbackQuery = PrimitiveQuery::create(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN);
    mGeneratedQuery = PrimitiveQuery::create();
    mFragmentsQuery = OcclusionQuery::create();
    mQueueA = ArrayBuffer::create();
    mQueueA->defineAttribute<uint32_t>("aPosition");
    mQueueA->bind().setData(std::vector<uint32_t>(maxQueueSize)); // preallocate
    mQueueB = ArrayBuffer::create();
    mQueueB->defineAttribute<uint32_t>("aPosition");
    mQueueB->bind().setData(std::vector<uint32_t>(maxQueueSize)); // preallocate
    mVAOQueueA = VertexArray::create(mQueueA, nullptr, GL_POINTS);
    mVAOQueueB = VertexArray::create(mQueueB, nullptr, GL_POINTS);
    mFeedbackA = TransformFeedback::create(mQueueA);
    mFeedbackB = TransformFeedback::create(mQueueB);
    for (auto const& s : {mInitShader, mMarchShader, mClearShader, mSliceShader})
    {
        s->setShaderStorageBuffer("bPoints", mPointBuffer);

        auto shader = s->use();

        shader.setImage(0, mTexDistanceField);
        shader.setImage(1, mTexNearestPoint);
        shader.setUniform("uMaxDistance", Size * 2.0f);
    }
    mInitShader->configureTransformFeedback({"gPosition"});
    mMarchShader->configureTransformFeedback({"gPosition"});

    // tweakbar
    TwAddVarRW(tweakbar(), "Crust Size", TW_TYPE_FLOAT, &mCrustSize, "group=algorithm step=0.1");

    TwAddVarRW(tweakbar(), "Cut Y", TW_TYPE_FLOAT, &mCutY, "group=rendering step=0.002");
    TwAddVarRW(tweakbar(), "Y Anim", TW_TYPE_FLOAT, &mYAnim, "group=rendering step=0.002");
    TwAddVarRW(tweakbar(), "Show Slice", TW_TYPE_BOOLCPP, &mShowSlice, "group=rendering");

    TwAddVarRW(tweakbar(), "Auto Recalc", TW_TYPE_BOOLCPP, &mRecalcEveryFrame, "group=actions");
    TwAddButton(tweakbar(), "Compute", BtnCompute, this, "group=actions");

    // cam init
    auto cam = getCamera();
    cam->setLookAt({1, 1, 1}, {0, 0, 0});

    // cpu df
    if (false)
    {
        std::vector<uint32_t> nearest;
        df::Grid3D<> grid(Size, Size, Size);
        df::nearest_field<df::GlmVec3Config>(mPoints, nearest, grid);
        mTexNearestPoint->bind().setData(GL_R32UI, Size, Size, Size, nearest);
        return;
    }

    // first refresh
    recompute();
}


void DistanceFieldSample::recompute()
{
    GLOW_SCOPED(enable, GL_RASTERIZER_DISCARD); // make the GS the last stage

    mFragmentsQuery->begin();
    mGeneratedQuery->begin();

    mGpuTimerStart->save();

    // clear
    mClearShader->use().compute(Size / CLEAR_SHADER_SIZE, Size / CLEAR_SHADER_SIZE, Size / CLEAR_SHADER_SIZE);

    // init
    {
        mFeedbackQuery->begin();
        mGpuTimer->begin();
        auto shader = mInitShader->use();

        auto vao = mPointVAO->bind();
        vao.negotiateBindings(); // must be done before transform feedback

        auto fb = mFeedbackA->bind();
        fb.begin(GL_POINTS);
        vao.draw();
        fb.end();
        mFeedbackQuery->end();
        mGpuTimer->end();
        auto time = TimerQuery::toSeconds(mGpuTimer->getResult64());
        auto pts = mFeedbackQuery->getResult64();
        glow::info() << "Seeds: " << pts << " (" << time * 1000 << " ms, " << (time / pts * 1000 * 1000 * 1000) << " ns/pt)";
    }
    glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

    // march
    auto it = 0;
    {
        auto outputA = false;

        auto shader = mMarchShader->use();
        shader.setUniform("uCrustSize", mCrustSize);
        mVAOQueueA->bind().negotiateBindings(); // must be done before transform feedback
        mVAOQueueB->bind().negotiateBindings(); // must be done before transform feedback

        auto shouldQuery = true;
        const int maxSync = 10;
        auto forceSync = maxSync;
        while (true)
        {
            if (shouldQuery)
            {
                mFeedbackQuery->begin();
                mGpuTimer->begin();
            }

            {
                auto fb = (outputA ? mFeedbackA : mFeedbackB)->bind();
                fb.begin(GL_POINTS);
                (outputA ? mVAOQueueB : mVAOQueueA)->bind().drawTransformFeedback(outputA ? mFeedbackB : mFeedbackA);
                fb.end();
            }
            glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

            if (shouldQuery)
            {
                mFeedbackQuery->end();
                mGpuTimer->end();
            }

            shouldQuery = false;
            if (mFeedbackQuery->isResultAvailable() || --forceSync < 0)
            {
                shouldQuery = true;
                auto pts = mFeedbackQuery->getResult64();
                auto time = TimerQuery::toSeconds(mGpuTimer->getResult64());
                glow::info() << "Got " << pts << " indices at iteration " << it << " (" << time * 1000 << " ms, "
                             << (time / pts * 1000 * 1000 * 1000) << " ns/pt)";

                forceSync = maxSync;
                (void)forceSync;

                if (pts == 0)
                    break;
            }

            outputA = !outputA;
            ++it;
        }
    }

    mGpuTimerEnd->save();
    mGeneratedQuery->end();
    mFragmentsQuery->end();
    auto timeSecs = mGpuTimerEnd->getSeconds() - mGpuTimerStart->getSeconds();

    auto totalCnt = mGeneratedQuery->getResult64();
    glow::info() << "Algorithm runtime: " << timeSecs * 1000 << " ms (" << it << " iterations)";
    glow::info() << "Considered " << totalCnt << " points (" << totalCnt / (float)(Size * Size * Size) * 100
                 << "% of cells, " << (timeSecs / totalCnt * 1000 * 1000 * 1000) << " ns/pt)";
    glow::info() << "Sanity check: " << mFragmentsQuery->getResult64() << " fragments";
}

void DistanceFieldSample::update(float elapsedSeconds)
{
    // IMPORTANT: call base function
    glfw::GlfwApp::update(elapsedSeconds);

    mCutY += mYAnim * elapsedSeconds;
}

void DistanceFieldSample::onRenderOpaquePass(glow::pipeline::RenderContext const& ctx)
{
    if (mShowSlice)
    {
        if (mRecalcEveryFrame)
            recompute();

        GLOW_SCOPED(disable, GL_CULL_FACE);

        auto shader = ctx.useProgram(mSliceShader);

        shader.setUniform("uCutY", mCutY);
        shader.setUniform("uCrustSize", mCrustSize);
        shader.setUniform("uView", ctx.camData.view);
        shader.setUniform("uProj", ctx.camData.proj);
        shader.setTexture("uTexDistanceField", mTexDistanceField);

        mQuadVAO->bind().draw();
    }
}
