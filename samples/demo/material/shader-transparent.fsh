#glow pipeline transparent

#include <glow-pipeline/pass/transparent/oitPass.glsl>
#include <glow-material/material-ggx.glsl>

uniform vec3 uCamPos;

uniform sampler2D uTextureColor;
uniform sampler2D uTextureNormal;
uniform float uTextureScale;

uniform vec3 uColor;
uniform float uMetallic;
uniform float uRoughness;
uniform float uAlpha;

uniform vec3 uLightDir;
uniform float uLightDistance;
uniform vec3 uLightColor;
uniform float uLightSize;
uniform float uLightRadius;

in vec3 vPosition;
in vec3 vNormal;
in vec3 vTangent;
in vec2 vTexCoord;

vec3 unpackNormalmap(vec3 rgb)
{
  return vec3(rgb.rg * 2 - 1 - 1 / 256, rgb.b);
}

void main() {
    vec3 N = normalize(vNormal);
    vec3 T = normalize(vTangent);
    vec3 B = normalize(cross(T, N));
    mat3 TBN = mat3(T, B, N);

    if (!gl_FrontFacing)
        discard;

    // texturing
    vec3 color = texture(uTextureColor, vTexCoord * uTextureScale).rgb * uColor;
    vec3 nmap = unpackNormalmap(texture(uTextureNormal, vTexCoord * uTextureScale).rgb);
    N = normalize(TBN * nmap);

    vec3 lightPos = uLightDir * uLightDistance;
    vec3 V = normalize(uCamPos - vPosition);
    vec3 R = 2 * dot(N, V) * N - V;

    // sphere lights
    vec3 L = lightPos - vPosition;
    float origLdis = length(L);
    vec3 c2ray = dot(L, R) * R - L;
    lightPos += c2ray * clamp(uLightSize / length(c2ray), 0.0, 1.0);
    L = normalize(lightPos - vPosition);

    float a = sqrt(uRoughness);
    float a2 = min(1.0, a + uLightSize / (2 * origLdis));
    float areaNormalization = pow(a / a2, 2);

    // attenuation (requires conversion to lumen!)
    float ldis = distance(lightPos, vPosition);
    float falloff = pow(max(0.0, 1 - pow(ldis / uLightRadius, 4)), 2) / (ldis * ldis + 1);
    falloff = 1;

    // ambient: TODO: irradiance map
    vec3 ambient = vec3(.01) * 0;

    // shading
    vec3 fcolor = ambient;
    fcolor += shadingGGX(N, V, L, uColor, uRoughness, uMetallic) * uLightColor * falloff * areaNormalization;
    fcolor += iblSpecularGGX(N, V, uColor, uRoughness, uMetallic);

    //outputFresnel(N, V, fcolor * uAlpha, uAlpha);
    outputOitGeometry(gl_FragCoord.z, fcolor, uAlpha);

    // TEST Checker Pattern
    // fColor *= float((uint(gl_FragCoord.x) + uint(gl_FragCoord.y)) % 2 == 0);
}
