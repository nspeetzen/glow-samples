uniform vec3 uColor;

in vec3 vPosition;
in vec3 vNormal;
in vec2 vTexCoord;

out vec3 fColor;

void main() {
    fColor = uColor;
}
