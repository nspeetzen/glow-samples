#pragma once

#include <vector>

#include <glm/ext.hpp>
#include <glm/glm.hpp>

#include <glow/fwd.hh>

#include <glow-extras/glfw/GlfwApp.hh>

class ParticleTracingSample : public glow::glfw::GlfwApp
{
public:
    ParticleTracingSample() : GlfwApp(Gui::AntTweakBar)
    {
        setWindowWidth(1280);
        setWindowHeight(1280);
    }

public:
    enum FlowType
    {
        Circular,   // Clockwise around the center
        Parallel,   // From left to right
        Converging, // Towards the center
        Diverging,  // Away from the center
        Wave,       // From left to right in waves
        Vortices,   // Four vortices, one source

        FlowTypeCount
    };

    enum Method
    {
        Euler,
        Midpoint,
        RK4,
        // TODO: RK_n?
        MethodCount
    };


private:
    // Global time
    float mTime = 0.0f;

    // Time step "h"
    float mDT = 0.1f;

    // Number of steps to simulate
    int mSteps = 16;

    // Velocity field resolution
    int mResX = 32;
    int mResY = 32;

    // Particle tail width
    float mTailWidth = 0.005f;

    // Arrow size in percent
    int mSize = 75;

    FlowType mFlowType = Circular;
    Method mMethod = Euler;

    glow::SharedProgram mShader;

    // flow field
    std::vector<glm::vec2> mVelocityField;
    glow::SharedVertexArray mArrowField;
    glow::SharedTexture2D mArrow;

    // particles
    std::vector<std::vector<glm::vec2>> mParticlePositions;
    glow::SharedVertexArray mParticlePoints;
    glow::SharedVertexArray mParticleTails;
    glow::SharedTexture2D mParticle;

    // UI
    std::vector<glm::vec2> mStartPositions = {{0.4, 0.6}};

    void generateFlowField();
    void generateBuffers();
    void simulateParticles();
    glm::vec2 integrate(glm::vec2 pos, float dt);
    glm::vec2 getVelocity(glm::vec2 pos);
    std::vector<glm::vec2> simulateOneParticle(glm::vec2 pos, float dt);

public:
    void recompute();

protected:
    void init() override;
    void update(float elapsedSeconds) override;
    void onResize(int w, int h) override;

    void onRenderOpaquePass(glow::pipeline::RenderContext const& ctx) override;

    bool onMousePosition(double x, double y) override;
    bool onMouseButton(double x, double y, int button, int action, int mods, int clickCount) override;
    bool onKey(int key, int scancode, int action, int mods) override;
};
