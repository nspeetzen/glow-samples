#include "uniforms.glsl"

in vec3 vPosition;
in float vLength;
in vec3 vNormal;

out vec3 fColor;

void main() {
    fColor = mix(uColor0, uColor1, smoothstep(0, 9, vLength));

    fColor *= abs(dot(normalize(vNormal), normalize(vec3(3,2,1))));
}
