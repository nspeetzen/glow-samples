// This file is auto-generated and should not be modified directly.
#include <nexus/test.hh>

#include <glm/glm.hpp>

#include <glow/objects/TextureCubeMapArray.hh>

using namespace glow;

TEST("TextureCubeMapArray, Binding")
{
    auto tex0 = TextureCubeMapArray::create();
    CHECK(TextureCubeMapArray::getCurrentTexture() == nullptr);

    {
        auto btex0 = tex0->bind();
        CHECK(TextureCubeMapArray::getCurrentTexture() == &btex0);

        auto tex1 = TextureCubeMapArray::create();
        auto tex2 = TextureCubeMapArray::create();
        CHECK(TextureCubeMapArray::getCurrentTexture() == &btex0);

        {
            auto btex1 = tex1->bind();
            CHECK(TextureCubeMapArray::getCurrentTexture() == &btex1);

            auto btex2 = tex2->bind();
            CHECK(TextureCubeMapArray::getCurrentTexture() == &btex2);
        }

        CHECK(TextureCubeMapArray::getCurrentTexture() == &btex0);
    }

    CHECK(TextureCubeMapArray::getCurrentTexture() == nullptr);
}
