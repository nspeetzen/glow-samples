// This file is auto-generated and should not be modified directly.
#include <nexus/test.hh>

#include <glm/glm.hpp>

#include <glow/objects/TextureBuffer.hh>

using namespace glow;

TEST("TextureBuffer, Binding")
{
    // TODO: implement TextureBuffer::resize
    return;

    auto tex0 = TextureBuffer::create();
    CHECK(TextureBuffer::getCurrentTexture() == nullptr);

    {
        auto btex0 = tex0->bind();
        CHECK(TextureBuffer::getCurrentTexture() == &btex0);

        auto tex1 = TextureBuffer::create();
        auto tex2 = TextureBuffer::create();
        CHECK(TextureBuffer::getCurrentTexture() == &btex0);

        {
            auto btex1 = tex1->bind();
            CHECK(TextureBuffer::getCurrentTexture() == &btex1);

            auto btex2 = tex2->bind();
            CHECK(TextureBuffer::getCurrentTexture() == &btex2);
        }

        CHECK(TextureBuffer::getCurrentTexture() == &btex0);
    }

    CHECK(TextureBuffer::getCurrentTexture() == nullptr);
}
