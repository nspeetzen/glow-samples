// This file is auto-generated and should not be modified directly.
#include <nexus/test.hh>

#include <glm/glm.hpp>

#include <glow/objects/TextureCubeMap.hh>

using namespace glow;

TEST("TextureCubeMap, Binding")
{
    auto tex0 = TextureCubeMap::create();
    CHECK(TextureCubeMap::getCurrentTexture() == nullptr);

    {
        auto btex0 = tex0->bind();
        CHECK(TextureCubeMap::getCurrentTexture() == &btex0);

        auto tex1 = TextureCubeMap::create();
        auto tex2 = TextureCubeMap::create();
        CHECK(TextureCubeMap::getCurrentTexture() == &btex0);

        {
            auto btex1 = tex1->bind();
            CHECK(TextureCubeMap::getCurrentTexture() == &btex1);

            auto btex2 = tex2->bind();
            CHECK(TextureCubeMap::getCurrentTexture() == &btex2);
        }

        CHECK(TextureCubeMap::getCurrentTexture() == &btex0);
    }

    CHECK(TextureCubeMap::getCurrentTexture() == nullptr);
}
