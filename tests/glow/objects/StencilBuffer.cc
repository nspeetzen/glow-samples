#include <nexus/test.hh>

#include <glow/objects/Framebuffer.hh>
#include <glow/objects/TextureRectangle.hh>

using namespace glow;

TEST("Stencil Buffer")
{
    for (auto fmt : {GL_DEPTH24_STENCIL8, //
                     GL_DEPTH32F_STENCIL8})
    {
        auto texColor = TextureRectangle::create(1, 1, GL_RGBA8);
        auto texDS = TextureRectangle::create(1, 1, fmt);

        auto fb = Framebuffer::create("fColor", texColor, texDS, texDS);
        CHECK(fb->bind().checkComplete());
    }

    for (auto fmt : {
             GL_STENCIL_INDEX,  //
                                // [cannot create] GL_STENCIL_INDEX1,
                                // [cannot create] GL_STENCIL_INDEX4,
             GL_STENCIL_INDEX8, //
             // [cannot create] GL_STENCIL_INDEX16,
         })
    {
        auto texColor = TextureRectangle::create(1, 1, GL_RGBA8);
        auto texStencil = TextureRectangle::create(1, 1, fmt);

        auto fb = Framebuffer::create("fColor", texColor, nullptr, texStencil);
        CHECK(fb->bind().checkComplete());
    }
}
