#include <nexus/test.hh>

#include <glm/glm.hpp>

#include <glow/objects/UniformBuffer.hh>
#include <glow/objects/Shader.hh>
#include <glow/objects/Program.hh>
#include <glow/objects/Texture2D.hh>

using namespace glow;

TEST("UniformBuffer, Binding")
{
    auto ub0 = UniformBuffer::create();
    CHECK(UniformBuffer::getCurrentBuffer() == nullptr);

    {
        auto bub0 = ub0->bind();
        CHECK(UniformBuffer::getCurrentBuffer() == &bub0);

        auto ub1 = UniformBuffer::create();
        auto ub2 = UniformBuffer::create();
        CHECK(UniformBuffer::getCurrentBuffer() == &bub0);

        {
            auto bub1 = ub1->bind();
            CHECK(UniformBuffer::getCurrentBuffer() == &bub1);

            auto bub2 = ub2->bind();
            CHECK(UniformBuffer::getCurrentBuffer() == &bub2);
        }

        CHECK(UniformBuffer::getCurrentBuffer() == &bub0);
    }

    CHECK(UniformBuffer::getCurrentBuffer() == nullptr);
}

TEST("UniformBuffer, Data")
{
    auto ub = UniformBuffer::create();
    auto bub = ub->bind();

    std::vector<int> data0 = {1, 2, 3};
    struct MyData
    {
        int a;
        float b;
        double c;
    } data1 = {1, 2, 3};

    bub.setData(data0.size(), data0.data());
    bub.setData(data0);
    bub.setData(data1);
}


static std::string csUBSource = R"src(#version 430 core

layout(local_size_x = 1, local_size_y = 1, local_size_z = 1) in;

uniform layout(rgba32f, binding=0) writeonly image2D uTex;

layout(std140) uniform ubData1 {
  float uSingle;
};

layout(std140) uniform ubData0 {
  float uArray[4];
};

void main() {
    uint x = gl_WorkGroupID.x;
    uint y = gl_WorkGroupID.y;
    imageStore(uTex, ivec2(x, y), vec4(x, y, uSingle, uArray[y % 4]));
}

)src";


TEST("UniformBuffer, Usage")
{
    std::vector<float> ubData = {5.f, 0, 0, 0, 6.f, 0, 0, 0, 7.f, 0, 0, 0, 8.f};
    auto ub0 = UniformBuffer::create();
    auto ub1 = UniformBuffer::create();
    ub0->bind().setData(ubData);

    auto cs = Shader::createFromSource(GL_COMPUTE_SHADER, csUBSource);
    CHECK(cs->isCompiledWithoutErrors());

    auto prog = Program::create(cs);
    prog->setUniformBuffer("ubData0", ub0);

    auto tex = Texture2D::createStorageImmutable(4, 4, GL_RGBA32F);

    {
        auto uprog = prog->use();

        uprog.setImage(0, tex, GL_WRITE_ONLY);

        prog->setUniformBuffer("ubData1", ub1);
        ub1->bind().setData(17.f);

        uprog.compute(tex->getWidth(), tex->getHeight());
    }

    auto data = tex->bind().getData<glm::vec4>();
    for (auto y = 0; y < tex->getHeight(); ++y)
        for (auto x = 0; x < tex->getWidth(); ++x)
        {
            auto val = data[y * tex->getWidth() + x];
            CHECK(val.x == x);
            CHECK(val.y == y);
            CHECK(val.z == 17.f);
            CHECK(val.w == 5.f + y);
        }
}
