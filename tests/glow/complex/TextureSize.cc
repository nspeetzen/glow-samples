#include <nexus/test.hh>

#include <glm/ext.hpp>
#include <glm/glm.hpp>

#include <glow/objects/Texture2DArray.hh>

TEST("TextureSize, StressTest")
{
    using namespace glow;
    int w = 3680;
    int h = 2456;
    int d = 100;
    while (d < 200)
    {
        auto tex0 = Texture2DArray::create(w, h, d, GL_RGB8);
        auto tex1 = Texture2DArray::create(w, h, d, GL_RGB8);
        auto tex2 = Texture2DArray::create(w, h, d, GL_RGB8);
        d += 10;
    }
}
