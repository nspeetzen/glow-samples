#version 430 core

in vec3 vPosition;

out vec3 fColor;

void main() {
    fColor = vPosition;
}
