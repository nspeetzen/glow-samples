#version 430 core

uniform sampler2DRect uTexIn;

out vec4 fOut;

uint wang_hash(uint seed)
{
    seed = (seed ^ 61) ^ (seed >> 16);
    seed *= 9;
    seed = seed ^ (seed >> 4);
    seed *= 0x27d4eb2d;
    seed = seed ^ (seed >> 15);
    return seed;
}

void main() {
    ivec2 size = textureSize(uTexIn);
    uint idx = uint(gl_FragCoord.x) + uint(gl_FragCoord.y) * uint(size.x);
    idx = wang_hash(idx);
    uint x = idx % size.x;
    uint y = (idx / size.x) % size.y;
    fOut = texture(uTexIn, vec2(x, y));
}
